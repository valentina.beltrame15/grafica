/**
 * -----------------------------------------------------------------------------
 * Computer Graphics - A.Y. 2020/2022
 * Teacher:  Antonino Casile
 * Students: Beltrame Valentina & Bongiovanni Valerio
 * ---------------------------------------------------------------------------------
 *
 * Assignment 06.1 - Sviluppare un programma OpenGL che, dati 4 punti di controllo, traccia una curva interpolante
 *                    e una BSplines in termini di una curva di Bezier cubica. In altre, parole, la curva interpolante
 *                    e la BSpline devono prima essere espresse in termini di una Bezier cubica che va quindi disegnata 
 *                    con le relative routine di OpenGL.

 * 
 * Solution : La soluzione consiste nel ricavare le matrici di conversione ed utilizzarle per calcolare la Matrice controlPoints
              interpolante o bspline.
 * NOTA- questo progetto è stato creato con visual studio code.
 *       Il comando per reanderlo operativo è :
 *       gcc assignment6.1completo.c -lglut -lGLU -lGL -lGLEW -lm
 *       mentre per avviarlo il comando è :
 *       ./a.out
              
    
 *
 */
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <stdio.h>
#include <math.h>
//commenta o rimuovi commento per cambiare tra curva interpolante e b-spline
#define interpolation
// Inizio variabili globali.
static int numVal = 0;

static float controlPoints[4][3] =
{
    {-30.0, 0.0, 0.0}, { -10.0, 20.0, 0.0},
    {10.0, -20.0, 0.0}, {30.0, 0.0, 0.0}
};


//Matrice di conversione ottenuta da M-bezier^-1 * M-interpolante
#ifdef interpolation
static float conversion[4][4] =
{
    {1.0,0.0,0.0,0.0},{-0.8333,3.0,-1.5,0.3333},{0.3333,-1.5,3.0,-0.8333},{0.0,0.0,0.0,1.0}
};
#else

//Matrice di conversione ottenuta da M-bezier^-1 * M-bsplines
static float conversion[4][4] =
{
    {0.1667, 0.6667, 0.1667 ,0.0}, {0.0, 0.6667, 0.3333, 0.0},
    {0.0, 0.3333, 0.6667, 0.0}, {0.0, 0.1667, 0.6667, 0.1667}
};
#endif

//Fine variabili globali

//Conversione dei punti di controllo Interpolanti/B-spline in punti di controllo di Bezier
void convertcp(float ctrlpoints[4][3]){
    int i,j,k;
    float tot = 0.0;
    for(i = 0; i < 4;i++){
        for(j = 0;j < 3;j++){
            ctrlpoints[i][j] = 0;
            for(k = 0; k < 4;k++){
                ctrlpoints[i][j] += conversion[i][k] * controlPoints[k][j];
            }
        }
    }
}

// Initialization routine.
void setup(void){glClearColor(1.0, 1.0, 1.0, 0.0);}


// Drawing routine.
void drawScene(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glColor3f(0.0, 0.0, 0.0);

    int indx;
    float ctrlpoints[4][3];
    convertcp(ctrlpoints);


    // Draw the control polygon in light gray
    glColor3f(0.7,0.7, 0.7);
    glBegin(GL_LINE_STRIP);
    for (indx = 0; indx < 4; indx++)
        glVertex3fv(controlPoints[indx]);
    glEnd();

    // Specify and enable the Bezier curve.
    glMap1f(GL_MAP1_VERTEX_3, 0.0, 1.0, 3, 4, &ctrlpoints[0][0]);
    glEnable(GL_MAP1_VERTEX_3);

    // Draw the Bezier curve by defining a sample grid and evaluating on it.
    glColor3f(0.0, 0.0, 0.0);
    glMapGrid1f(100, 0.0, 1.0);
    glEvalMesh1(GL_LINE, 0, 100);

    // Draw the Bezier control points as dots.
    glPointSize(5.0);
    glColor3f(0.0, 1.0, 0.0);
    glBegin(GL_POINTS);
    for (indx = 0; indx < 4; indx++)
        glVertex3fv(controlPoints[indx]);
    glEnd();

    // Highlight selected control point,
    glColor3f(1.0, 0.0, 0.0);
    glBegin(GL_POINTS);
    glVertex3fv(controlPoints[numVal]);
    glEnd();

    glutSwapBuffers();
}

// OpenGL window reshape routine.
void resize(int w, int h)
{
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-50.0, 50.0, -50.0, 50.0, -1.0, 1.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

// Keyboard input processing routine.
void keyInput(unsigned char key, int x, int y)
{
    switch (key)
    {
    case 27:
        exit(0);
        break;
    case ' ':
        if (numVal < 3) numVal++;
            else numVal = 0;
        glutPostRedisplay();
        break;
    default:
        break;
    }
}

// Callback routine for non-ASCII key entry.
void specialKeyInput(int key, int x, int y)
{

    if(key == GLUT_KEY_UP) controlPoints[numVal][1] += 1.0;
    if(key == GLUT_KEY_DOWN) controlPoints[numVal][1] -= 1.0;
    if(key == GLUT_KEY_LEFT) controlPoints[numVal][0] -= 1.0;
    if(key == GLUT_KEY_RIGHT) controlPoints[numVal][0] += 1.0;
    glutPostRedisplay();
}

// Routine to output interaction instructions to the console window.
void printInteraction(void)
{
    printf("Interaction:\n");
    printf("Press space to select a control point.\n");
    printf("Press the arrow keys to move the selected control point.\n");
}

// Main routine.
int main(int argc, char **argv)
{

    printInteraction();
    glutInit(&argc, argv);

    glutInitContextVersion(4, 3);
    glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);

    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowSize(500, 500);
    glutInitWindowPosition(100, 100);
    #ifdef interpolation
    glutCreateWindow("Interpolation Curve");
    #else
    glutCreateWindow("B-spline Curve");
    #endif
    glutDisplayFunc(drawScene);
    glutReshapeFunc(resize);
    glutKeyboardFunc(keyInput);
    glutSpecialFunc(specialKeyInput);

    glewExperimental = GL_TRUE;
    glewInit();

    setup();

    glutMainLoop();
}
