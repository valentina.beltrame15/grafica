/**
 * -----------------------------------------------------------------------------
 * Computer Graphics - A.Y. 2021/2022
 * Teacher:  Antonino Casile
 * Students: Beltrame Valentina & Valerio Bongiovanni
 * -----------------------------------------------------------------------------
 * Assignment 02 - Stimare, con il codice dato a lezione, la dipendenza dei FPS dal numero di linee e
                   produrre a schermo un grafico rudimentale della curva che ne risulta
                   Ripetere l’esercizio nel caso in cui le linee non abbiano un colore uniforme.
 * Solution:       Utilizzo due cicli concatenati per effettuare le stime di dipendenza,
                   una terminati i test mostra a schermo la dipendenza in un grafico.

    * NOTA- questo progetto è stato creato con visual studio code.
 *       Il comando per reanderlo operativo è :
 *       gcc assignment2completo.c -lglut -lGLU -lGL -lGLEW -lm
 *       mentre per avviarlo il comando è :
 *       ./a.out
 * ---------------------------------------------------------------------------------
 */
#include <GL/glut.h>
#include <unistd.h>
#include <time.h>
#include <stdio.h>


// Commenta per rimuovere i colori, questo define serve per definire i colori
 #define color1 

// Number of elements in the circular buffer
#define NELS 10

// Number of test
#define NTEST 10
#define NCICLI 100

// Number of lines
int NLINES = 10;

// circular buffer used to compute frame rate
float circularBuffer[NELS];
float FPSOut[NTEST];
int lineOut[NTEST];
int firstInd = 0, nEls = 0;

// function to get the number of elapsed ticks
uint32_t getTick()
{
    struct timespec ts;
    unsigned theTick = 0U;
    clock_gettime( CLOCK_REALTIME, &ts );
    theTick  = ts.tv_nsec / 1000000;
    theTick += ts.tv_sec * 1000;
    return theTick;
}

// Function to compute real modulus and NOT simply the remainder as % does
int modulo(int a, int b) {
    const int result = a % b;
    return result >= 0 ? result : result + b;
}

// Compute sum of the elements in the circular buffer
float sumCircularBuffer()
{
    int ind;
    float sum = 0;

    if (nEls > 0) {
        for (ind=1; ind<=nEls; ind++) {
            sum = sum + circularBuffer[modulo(firstInd-ind, NELS)];
        }
    }

    return sum;
}

// accumulate buffer and update window title
void computeAndShowFrameRate(void)
{
    static float lastTime = 0.0f;
    static unsigned int frameCount = 0;
    char windowTitle[100];
    float sumFPS;
    
    float currentTime = (float)getTick() * 0.001f;
    // Initialize lastTime to the current time
    if (lastTime == 0) {
        lastTime = currentTime;
    }

    // increase frame count
    frameCount++;
    if (currentTime - lastTime > 1.0f) {
        // insert the current fps in the circular buffer
        circularBuffer[firstInd] = ((float)frameCount) / (currentTime - lastTime);

        // update variable lastTime
        lastTime = currentTime;

        //circularBuffer[firstInd] = (float)frameCount;
        firstInd = ((firstInd+1)%NELS);
        if (nEls < NELS) {
            nEls++;
        }
        frameCount = 0;

        // sum elements in circular buffer
        sumFPS = sumCircularBuffer();
        snprintf(windowTitle, 100, "FPS = %6.2f", sumFPS/nEls);
        // update window title
        glutSetWindowTitle(windowTitle);
    }
}

// display function
void display(void)
{
    int currLineInd, kcicli = 0;
    for(int j = 0; j < NTEST; j++){
        while(nEls < NELS | kcicli < NCICLI){
            // get current frame rate
            computeAndShowFrameRate();

            // clear buffer
            glClear (GL_COLOR_BUFFER_BIT);

            for (currLineInd = 0; currLineInd<NLINES; currLineInd++) {
                // draw line
                glBegin(GL_LINES);
                
                // random color
#ifdef color1
                glColor3f(1.0,0.0,0.0);
#else
                glColor3f((float)rand()/RAND_MAX, (float)rand()/RAND_MAX, (float)rand()/RAND_MAX);
#endif
                // random first point
                glVertex2f((float)rand()/RAND_MAX, (float)rand()/RAND_MAX);
                // random color
                //glColor3f((float)rand()/RAND_MAX, (float)rand()/RAND_MAX, (float)rand()/RAND_MAX);
                // random second point
                glVertex2f((float)rand()/RAND_MAX, (float)rand()/RAND_MAX);
                glEnd();
            }
            glFinish();
            //glutPostRedisplay();
            kcicli++;
        }
        //salvo gli fps risultanti in un array
        FPSOut[j] = sumCircularBuffer()/nEls;
        //salvo il numero di linee stampate
        lineOut[j] = NLINES;
        //stampo a video il numero di linee e gli fps
        printf("NLINES = %d\tFPS = %f\n",NLINES,FPSOut[j]);
        nEls = 0;
        //aumento progressivamente il numero di linee da stampare a video
        NLINES = NLINES * 2;
    }
    // clear buffer
    glClear (GL_COLOR_BUFFER_BIT);

    //cambio il titolo alla finestra per il grafico
    glutSetWindowTitle("FPS in funzione di NLINES");
    for (int i = 0; i<NTEST-1; i++) {
        // draw line
        glBegin(GL_LINES);
        // random color
        glColor3f(1.0, 1.0, 1.0);
        //per stampare la curva a video utilizzo come x il numero di linee e come y gli fps
        // random first point
        glVertex2f(1.0/lineOut[NTEST-1]*lineOut[i], 1.0/FPSOut[1]*FPSOut[i]);
        // random second point
        glVertex2f(1.0/lineOut[NTEST-1]*lineOut[i+1], 1.0/FPSOut[1]*FPSOut[i+1]);
        glEnd();
    }
    glFinish();
}

// initialization function
void init (void)
{
    // Use current time as seed for random generator
    srand(time(0));

    // select clearing color
    glClearColor (0.0, 0.0, 0.0, 0.0);

    // Orthographic projection
    glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
}

// Window size and mode
int main(int argc, char** argv)
{
    // pass potential input arguments to glutInit
    glutInit(&argc, argv);

    // set display mode
    // GLUT_SINGLE = single buffer window
    glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);

    glutInitWindowSize (400, 400);
    glutInitWindowPosition (100, 100);
    glutCreateWindow ("OpenGL Window");

    // Call initialization routinesx
    init();
    glutDisplayFunc(display);
    glutMainLoop();
    return 0;
}
