/**
 * -----------------------------------------------------------------------------
 * Computer Graphics - A.Y. 2021/2022
 * Teacher:  Antonino Casile
 * Students: Beltrame Valentina & Valerio Bongiovanni
 * -----------------------------------------------------------------------------
 * Assignment 04 -1 - Modellare il sistema solare mettendo il sole come fonte di luce
                2 - Modellare i periodi di rivoluzione dei pianeti e le loro grandezze relative (ma non le loro distanze relative dal sole)
                Mettere i nomi accanto ad ogni pianeta
 * Solution:    Ho considerato il sole come un punto che emette luce.

 * NOTA- questo progetto è stato creato con visual studio code.
 *       Il comando per reanderlo operativo è :
 *       gcc assignment4completo.c -lglut -lGLU -lGL -lGLEW -lm
 *       mentre per avviarlo il comando è :
 *       ./a.out
 * ---------------------------------------------------------------------------------*/





#include <GL/glew.h>
#include <GL/freeglut.h>
#include <GL/glut.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

/* Number of planets. */
#define NPLANETS 8



/* Define a planet struct containing information about the planets. */
typedef struct {
    char    name[10];           // The name of the planet.
    GLdouble radius;             // The relative planet dimensions depend on the planet radius (x10^3).
    GLdouble trajectoryRadius;   // Planet's circular trajectory radius.
    GLdouble revolutionStep;     // 1.0 / number of days to complete an orbit revolution.
    GLdouble currentStep;        // Current step of the interpolation for each orbit.
} Planet;


Planet P1, P2, P3, P4 ,P5 ,P6 ,P7 ,P8;


//servono per calcolare la rivoluzione intorno al sole
GLdouble x,y;





/* Time interval on which update the planets position. */
unsigned int interval = 500;

//calcoliamo le rivoluzioni dei pianeti
void revolution(int value){
    switch (value){
    case 1:
        P1.currentStep= P1.currentStep + P1.revolutionStep;
        
        glutPostRedisplay();
  
    break;

    case 2:
        P2.currentStep= P2.currentStep + P2.revolutionStep;

        
        glutPostRedisplay();
  
    break;

    case 3:
        P3.currentStep= P3.currentStep + P3.revolutionStep;
        
        glutPostRedisplay();
  
    break;

    case 4:
        P4.currentStep= P4.currentStep + P4.revolutionStep;
        
        glutPostRedisplay();
  
    break;


    case 5:
        P5.currentStep= P5.currentStep + P5.revolutionStep;
        
        glutPostRedisplay();
  
    break;

    case 6:
        P6.currentStep= P6.currentStep + P6.revolutionStep;
        
        glutPostRedisplay();
  
    break;

    case 7:
        P7.currentStep= P7.currentStep + P7.revolutionStep;
        
        glutPostRedisplay();
  
    break;

    case 8:
        P8.currentStep= P8.currentStep + P8.revolutionStep;
        
        glutPostRedisplay();
  
    break;

    }
    


    
}
void display(){
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    //Mercurio


        glutTimerFunc(interval, revolution,1);
        x=0.5/2*sin(P1.currentStep)*P1.trajectoryRadius;
        y=0.5/2*cos(P1.currentStep)*P1.trajectoryRadius;

        glPushMatrix();
        glColor3f(0.5, 0.40, 0.05); //marrone
        glTranslatef(x, y, -2.5);
        glutSolidSphere(P1.radius, 1000, 1000);
        
        glColor3f(1.0, 1.0, 1.0);
        glRasterPos2d(-0.48,0.25);
        
        glutBitmapString(GLUT_BITMAP_HELVETICA_10,P1.name);
    
       

        glPopMatrix(); 

    
    

    //Venere
   

    glutTimerFunc(interval, revolution,2);
        x=0.5/2*sin(P2.currentStep)*P2.trajectoryRadius;
        y=0.5/2*cos(P2.currentStep)*P2.trajectoryRadius;
    glPushMatrix();
        glColor3f(0.0, 1.0, 0.0); //verde
        glTranslatef(x, y, -2.5);
        glutSolidSphere(P2.radius, 1000, 1000);
        
        glColor3f(1.0, 1.0, 1.0);
        glRasterPos2d(-0.38,0.32);
        glutBitmapString(GLUT_BITMAP_HELVETICA_10,P2.name);

    glPopMatrix();

    //Terra
    
    glutTimerFunc(interval, revolution,3);
        x=0.5/2*sin(P3.currentStep)*P3.trajectoryRadius;
        y=0.5/2*cos(P3.currentStep)*P3.trajectoryRadius;

    glPushMatrix();
        glColor3f(0.0, 0.0, 1.0); //blu
        glTranslatef(x, y, -2.5);
        glutSolidSphere(P3.radius, 1000, 1000);
        
        glColor3f(1.0, 1.0, 1.0);
        glRasterPos2d(-0.28,0.32);
        glutBitmapString(GLUT_BITMAP_HELVETICA_10,P3.name);
    glPopMatrix();


    

    //Marte
    
    glutTimerFunc(interval, revolution,4);
        x=0.5/2*sin(P4.currentStep)*P4.trajectoryRadius;
        y=0.5/2*cos(P4.currentStep)*P4.trajectoryRadius;

    glPushMatrix();
        glColor3f(1.0, 0.0, 0.0); //rosso
        glTranslatef(x, y, -2.5);
        glutSolidSphere(P4.radius, 1000, 1000);
        
        glColor3f(1.0, 1.0, 1.0);
        glRasterPos2d(-0.28,0.25);
        glutBitmapString(GLUT_BITMAP_HELVETICA_10,P4.name);
    glPopMatrix();

    //Giove
    
    glutTimerFunc(interval, revolution,5);
        x=0.5/2*sin(P5.currentStep)*P5.trajectoryRadius;
        y=0.5/2*cos(P5.currentStep)*P5.trajectoryRadius;

    glPushMatrix();
        glColor3f(1.0, 0.4, 0.0); //arancio
        glTranslatef(x, y, -2.5);
        glutSolidSphere(P5.radius, 1000, 1000);

        glColor3f(1.0, 1.0, 1.0);
        glRasterPos2d(-0.28,0.50);
        glutBitmapString(GLUT_BITMAP_HELVETICA_10,P5.name);
    glPopMatrix();

    //Saturno
   
    glutTimerFunc(interval, revolution,6);
        x=0.5/2*sin(P6.currentStep)*P6.trajectoryRadius;
        y=0.5/2*cos(P6.currentStep)*P6.trajectoryRadius;

    glPushMatrix();
        glColor3f(0.5, 0.5, 0.5); //grigio
        glTranslatef(x, y, -2.5);
        glutSolidSphere(P6.radius, 1000, 1000);

        glColor3f(1.0, 1.0, 1.0);
        glRasterPos2d(-0.38,0.50);
        glutBitmapString(GLUT_BITMAP_HELVETICA_10,P6.name);
    glPopMatrix();

    //Urano
    
    glutTimerFunc(interval, revolution,7);
        x=0.5/2*sin(P7.currentStep)*P7.trajectoryRadius;
        y=0.5/2*cos(P7.currentStep)*P7.trajectoryRadius;

    glPushMatrix();
        glColor3f(0.2, 0.7, 1.0); //azzurro
        glTranslatef(x, y, -2.5);
        glutSolidSphere(P7.radius, 1000, 1000);

        glColor3f(1.0, 1.0, 1.0);
        glRasterPos2d(-0.28,0.35);
        glutBitmapString(GLUT_BITMAP_HELVETICA_10,P7.name);
    glPopMatrix();

    //Nettuno
    

    
    glutTimerFunc(interval, revolution,8);
        x=0.5/2*sin(P8.currentStep)*P8.trajectoryRadius;
        y=0.5/2*cos(P8.currentStep)*P8.trajectoryRadius;

    glPushMatrix();
        glColor3f(0.5, 0.0, 1.0); //viola
        glTranslatef(x, y, -2.5);
        glutSolidSphere(P8.radius, 1000, 1000);

        glColor3f(1.0, 1.0, 1.0);
        glRasterPos2d(-0.38,0.38);
        glutBitmapString(GLUT_BITMAP_HELVETICA_10,P8.name);
    glPopMatrix();

    

    glFlush();
}

void init(){
    strcpy(P1.name, "MERCURIO");
    P1.radius = 0.10;
    P1.trajectoryRadius = 1.0;
    P1.revolutionStep = 0.114;
    P1.currentStep = 0.0;

     strcpy(P2.name, "VENERE");
    P2.radius = 0.15;
    P2.trajectoryRadius = 3.0;
    P2.revolutionStep = 0.044;
    P2.currentStep = 0;

    strcpy(P3.name, "TERRA");
    P3.radius = 0.15;
    P3.trajectoryRadius = 5.5;
    P3.revolutionStep = 0.027;
    P3.currentStep = 0;

    strcpy(P4.name, "MARTE");
    P4.radius = 0.10;
    P4.trajectoryRadius = 9.5;
    P4.revolutionStep = 0.015;
    P4.currentStep = 0;

    strcpy(P5.name, "GIOVE");
    P5.radius = 0.22;
    P5.trajectoryRadius = 14.5;
    P5.revolutionStep = 0.002;
    P5.currentStep = 0;


    strcpy(P6.name, "SATURNO");
    P6.radius = 0.21;
    P6.trajectoryRadius = 18.0;
    P6.revolutionStep = 0.001;
    P6.currentStep = 0;

    strcpy(P7.name, "URANO");
    P7.radius = 0.15;
    P7.trajectoryRadius = 21.0;
    P7.revolutionStep = 0.0003;
    P7.currentStep = 0;


    strcpy(P8.name, "NETTUNO");
    P8.radius = 0.15;
    P8.trajectoryRadius = 25.0;
    P8.revolutionStep = 0.0002;
    P8.currentStep = 0;



    glClearColor(0.0, 0.0, 0.0, 0.0);
    //glOrtho(-15.0, 15.0, -10.0, 10.0, 1.0, 10.0);
    glFrustum(-4.0, 4.0, -3.0, 3.0, 1.0, 3.0);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial  (  GL_FRONT_AND_BACK ,  GL_AMBIENT_AND_DIFFUSE  );

    GLfloat lightpos[] = {0.0, 0.0,-2.5 , 0.0};
    GLfloat spec[] = {3.0, 3.0, 3.0, 0.0};
    glLightfv(GL_LIGHT0,GL_DIFFUSE, spec);
    glLightfv(GL_LIGHT0, GL_POSITION, lightpos);
}



int main(int argc, char** argv){
    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowPosition(1000,1000);
    glutInitWindowSize(1900, 1000);
    glutCreateWindow("Sistema solare");


    init();
    glutDisplayFunc(display);
    glutMainLoop();
    return 0; 
}