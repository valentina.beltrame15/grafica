/**
 * -----------------------------------------------------------------------------
 * Computer Graphics - A.Y. 2021/2022
 * Teacher:  Antonino Casile
 * Students: Beltrame Valentina & Valerio Bongiovanni
 * -----------------------------------------------------------------------------
 * Assignment 03 -Modellare un cubo a 4 facce in cui ogni faccia abbia un buco
                  quadrato centrale. Scrivere 3 versioni del programma OpenGL che ne fa il
                  rendering a video:
                  1 – Tramite direttive GL_TRIANGLES
                  2 e 3 – Tramite DrawArrays con e senza VBOs
 * Solution:      Questo programma consente di disegnare un cubo di 4 facce,
 *                ciascuna con un foro quadrato al centro di essa.
 *                Il programma consente di realizzare il disegno con 3 "tecniche":
 *                      1. direttiva GL_TRIANGLES
 *                      2. direttiva glDrawArrays
 *                      3. direttiva glDrawArrays implementata tramite Vertex Buffer Object
 * 
 * NOTA- questo progetto è stato creato con visual studio code.
 *       Il comando per reanderlo operativo è :
 *       gcc assignment3completo.c -lglut -lGLU -lGL -lGLEW -lm
 *       mentre per avviarlo il comando è :
 *       ./a.out
 * ---------------------------------------------------------------------------------*/

#include <GL/glew.h>
#include <GL/glut.h>
#include <unistd.h>
#include <stdio.h>

#define NFACES 4
#define VERTEX_PER_FACES 17

#define ARRAY
//#define VBO

#ifdef ARRAY
//primi due vertici sono uguali agli ultimi due in modo da poter chiudere la forma
GLfloat cube[3*VERTEX_PER_FACES*NFACES] = {
    // faccia davanti
    /**/-1.5, -1.5, -0.5,
    -0.5, -0.5, -0.5,
    -1.5, -0.5, -0.5,
    -0.5, +0.5, -0.5,
    -1.5, +1.5, -0.5,
    -0.5, +0.5, -0.5, // aggiunto per far funzionare GL_TRIANGLE_STRIP
    -0.5, +1.5, -0.5,
    +0.5, +0.5, -0.5,
    +1.5, +1.5, -0.5,
    +0.5, +0.5, -0.5, // aggiunto per far funzionare GL_TRIANGLE_STRIP
    +1.5, +0.5, -0.5,
    +0.5, -0.5, -0.5,
    +1.5, -1.5, -0.5,
    +0.5, -0.5, -0.5, // aggiunto per far funzionare GL_TRIANGLE_STRIP
    +0.5, -1.5, -0.5,
    -0.5, -0.5, -0.5,
    -1.5, -1.5, -0.5,/**/
    // faccia sx
    /**/-1.5, -1.5, -2.0,
    -1.5, -0.5, -1.5,
    -1.5, -0.5, -2.0,
    -1.5, +0.5, -1.5,
    -1.5, +1.5, -2.0,
    -1.5, +0.5, -1.5, // aggiunto per far funzionare GL_TRIANGLE_STRIP
    -1.5, +1.5, -1.5,
    -1.5, +0.5, -1.0,
    -1.5, +1.5, -0.5,
    -1.5, +0.5, -1.0, // aggiunto per far funzionare GL_TRIANGLE_STRIP
    -1.5, +0.5, -0.5,
    -1.5, -0.5, -1.0,
    -1.5, -1.5, -0.5,
    -1.5, -0.5, -1.0, // aggiunto per far funzionare GL_TRIANGLE_STRIP
    -1.5, -1.5, -1.0,
    -1.5, -0.5, -1.5,
    -1.5, -1.5, -2.0,/**/
    // faccia dx
    /**/+1.5, -1.5, -2.0,
    +1.5, -0.5, -1.5,
    +1.5, -0.5, -2.0,
    +1.5, +0.5, -1.5,
    +1.5, +1.5, -2.0,
    +1.5, +0.5, -1.5, // aggiunto per far funzionare GL_TRIANGLE_STRIP
    +1.5, +1.5, -1.5,
    +1.5, +0.5, -1.0,
    +1.5, +1.5, -0.5,
    +1.5, +0.5, -1.0, // aggiunto per far funzionare GL_TRIANGLE_STRIP
    +1.5, +0.5, -0.5,
    +1.5, -0.5, -1.0,
    +1.5, -1.5, -0.5,
    +1.5, -0.5, -1.0, // aggiunto per far funzionare GL_TRIANGLE_STRIP
    +1.5, -1.5, -1.0,
    +1.5, -0.5, -1.5,
    +1.5, -1.5, -2.0,/**/
    //faccia dietro
    /**/-1.5, -1.5, -2.0,
    -0.5, -0.5, -2.0,
    -1.5, -0.5, -2.0,
    -0.5, +0.5, -2.0,
    -1.5, +1.5, -2.0,
    -0.5, +0.5, -2.0, // aggiunto per far funzionare GL_TRIANGLE_STRIP
    -0.5, +1.5, -2.0,
    +0.5, +0.5, -2.0,
    +1.5, +1.5, -2.0,
    +0.5, +0.5, -2.0, // aggiunto per far funzionare GL_TRIANGLE_STRIP
    +1.5, +0.5, -2.0,
    +0.5, -0.5, -2.0,
    +1.5, -1.5, -2.0,
    +0.5, -0.5, -2.0, // aggiunto per far funzionare GL_TRIANGLE_STRIP
    +0.5, -1.5, -2.0,
    -0.5, -0.5, -2.0,
    -1.5, -1.5, -2.0/**/
};

GLfloat colorCube[3*VERTEX_PER_FACES*NFACES] = {
    // faccia davanti
    /**/1.0, 0.0, 0.0,
    1.0, 0.0, 0.0,
    1.0, 0.0, 0.0,
    1.0, 0.0, 0.0,
    1.0, 0.0, 0.0,
    1.0, 0.0, 0.0,
    1.0, 0.0, 0.0,
    1.0, 0.0, 0.0,
    1.0, 0.0, 0.0,
    1.0, 0.0, 0.0,
    1.0, 0.0, 0.0,
    1.0, 0.0, 0.0,
    1.0, 0.0, 0.0,
    1.0, 0.0, 0.0,
    1.0, 0.0, 0.0,
    1.0, 0.0, 0.0,
    1.0, 0.0, 0.0,/**/
    // faccia sx
    /**/0.0, 1.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 1.0, 0.0,/**/
    // faccia dx
    0.0, 0.0, 1.0,
    0.0, 0.0, 1.0,
    0.0, 0.0, 1.0,
    0.0, 0.0, 1.0,
    0.0, 0.0, 1.0,
    0.0, 0.0, 1.0,
    0.0, 0.0, 1.0,
    0.0, 0.0, 1.0,
    0.0, 0.0, 1.0,
    0.0, 0.0, 1.0,
    0.0, 0.0, 1.0,
    0.0, 0.0, 1.0,
    0.0, 0.0, 1.0,
    0.0, 0.0, 1.0,
    0.0, 0.0, 1.0,
    0.0, 0.0, 1.0,
    0.0, 0.0, 1.0,
    //faccia dietro
    /**/0.5, 0.0, 0.5,
    0.5, 0.0, 0.5,
    0.5, 0.0, 0.5,
    0.5, 0.0, 0.5,
    0.5, 0.0, 0.5,
    0.5, 0.0, 0.5,
    0.5, 0.0, 0.5,
    0.5, 0.0, 0.5,
    0.5, 0.0, 0.5,
    0.5, 0.0, 0.5,
    0.5, 0.0, 0.5,
    0.5, 0.0, 0.5,
    0.5, 0.0, 0.5,
    0.5, 0.0, 0.5,
    0.5, 0.0, 0.5,
    0.5, 0.0, 0.5,
    0.5, 0.0, 0.5/**/
};

#ifdef VBO
GLuint buffer;
#endif
#endif

// display function
void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // store modelview settings
    glPushMatrix();
    glTranslatef(0.0, 0.0, -1.5);
    glTranslatef(0.0, -2.8, 0.0);
    glRotatef(20.0, 0.0, 1.0, 0.0);
    glRotatef(20.0, 1.0, 0.0, 0.0);

    #ifdef ARRAY
        for(int indFaces = 0; indFaces < NFACES; indFaces++){
            glDrawArrays(GL_TRIANGLE_STRIP, indFaces*VERTEX_PER_FACES,VERTEX_PER_FACES);
        }
    #else
        glBegin(GL_TRIANGLES);
        // faccia davanti
        glColor3f(1.0, 0.0, 0.0);
        glVertex3f(-1.5, -1.5, -0.5);
        glVertex3f(-0.5, -0.5, -0.5);
        glVertex3f(-1.5, -0.5, -0.5);

        glVertex3f(-0.5, +0.5, -0.5);
        glVertex3f(-0.5, -0.5, -0.5);
        glVertex3f(-1.5, -0.5, -0.5);

        glVertex3f(-0.5, +0.5, -0.5);
        glVertex3f(-1.5, +1.5, -0.5);
        glVertex3f(-1.5, -0.5, -0.5);

        glVertex3f(-0.5, +0.5, -0.5);
        glVertex3f(-1.5, +1.5, -0.5);
        glVertex3f(-0.5, +1.5, -0.5);

        glVertex3f(-0.5, +0.5, -0.5);
        glVertex3f(+0.5, +0.5, -0.5);
        glVertex3f(-0.5, +1.5, -0.5);

        glVertex3f(+1.5, +1.5, -0.5);
        glVertex3f(+0.5, +0.5, -0.5);
        glVertex3f(-0.5, +1.5, -0.5);

        glVertex3f(+1.5, +1.5, -0.5);
        glVertex3f(+0.5, +0.5, -0.5);
        glVertex3f(+1.5, +0.5, -0.5);

        glVertex3f(+0.5, -0.5, -0.5);
        glVertex3f(+0.5, +0.5, -0.5);
        glVertex3f(+1.5, +0.5, -0.5);

        glVertex3f(+0.5, -0.5, -0.5);
        glVertex3f(+1.5, -1.5, -0.5);
        glVertex3f(+1.5, +0.5, -0.5);

        glVertex3f(+0.5, -0.5, -0.5);
        glVertex3f(+1.5, -1.5, -0.5);
        glVertex3f(+0.5, -1.5, -0.5);

        glVertex3f(+0.5, -0.5, -0.5);
        glVertex3f(-0.5, -0.5, -0.5);
        glVertex3f(+0.5, -1.5, -0.5);

        glVertex3f(+0.5, -1.5, -0.5);
        glVertex3f(-0.5, -0.5, -0.5);
        glVertex3f(-1.5, -1.5, -0.5);
        
        //faccia dietro
        glColor3f(0.5, 0.0, 0.5);
        glVertex3f(-1.5, -1.5, -2.0);
        glVertex3f(-0.5, -0.5, -2.0);
        glVertex3f(-1.5, -0.5, -2.0);

        glVertex3f(-0.5, +0.5, -2.0);
        glVertex3f(-0.5, -0.5, -2.0);
        glVertex3f(-1.5, -0.5, -2.0);

        glVertex3f(-0.5, +0.5, -2.0);
        glVertex3f(-1.5, +1.5, -2.0);
        glVertex3f(-1.5, -0.5, -2.0);

        glVertex3f(-0.5, +0.5, -2.0);
        glVertex3f(-1.5, +1.5, -2.0);
        glVertex3f(-0.5, +1.5, -2.0);

        glVertex3f(-0.5, +0.5, -2.0);
        glVertex3f(+0.5, +0.5, -2.0);
        glVertex3f(-0.5, +1.5, -2.0);

        glVertex3f(+1.5, +1.5, -2.0);
        glVertex3f(+0.5, +0.5, -2.0);
        glVertex3f(-0.5, +1.5, -2.0);

        glVertex3f(+1.5, +1.5, -2.0);
        glVertex3f(+0.5, +0.5, -2.0);
        glVertex3f(+1.5, +0.5, -2.0);

        glVertex3f(+0.5, -0.5, -2.0);
        glVertex3f(+0.5, +0.5, -2.0);
        glVertex3f(+1.5, +0.5, -2.0);

        glVertex3f(+0.5, -0.5, -2.0);
        glVertex3f(+1.5, -1.5, -2.0);
        glVertex3f(+1.5, +0.5, -2.0);

        glVertex3f(+0.5, -0.5, -2.0);
        glVertex3f(+1.5, -1.5, -2.0);
        glVertex3f(+0.5, -1.5, -2.0);

        glVertex3f(+0.5, -0.5, -2.0);
        glVertex3f(-0.5, -0.5, -2.0);
        glVertex3f(+0.5, -1.5, -2.0);

        glVertex3f(+0.5, -1.5, -2.0);
        glVertex3f(-0.5, -0.5, -2.0);
        glVertex3f(-1.5, -1.5, -2.0);

        //faccia sx
        glColor3f(0.0, 1.0, 0.0);
        glVertex3f(-1.5, -1.5, -2.0);
        glVertex3f(-1.5, -0.5, -1.5);
        glVertex3f(-1.5, -0.5, -2.0);

        glVertex3f(-1.5, +0.5, -1.5);
        glVertex3f(-1.5, -0.5, -1.5);
        glVertex3f(-1.5, -0.5, -2.0);

        glVertex3f(-1.5, +0.5, -1.5);
        glVertex3f(-1.5, +1.5, -2.0);
        glVertex3f(-1.5, -0.5, -2.0);

        glVertex3f(-1.5, +0.5, -1.5);
        glVertex3f(-1.5, +1.5, -2.0);
        glVertex3f(-1.5, +1.5, -1.5);

        glVertex3f(-1.5, +0.5, -1.5);
        glVertex3f(-1.5, +0.5, -1.0);
        glVertex3f(-1.5, +1.5, -1.5);

        glVertex3f(-1.5, +1.5, -0.5);
        glVertex3f(-1.5, +0.5, -1.0);
        glVertex3f(-1.5, +1.5, -1.5);

        glVertex3f(-1.5, +1.5, -0.5);
        glVertex3f(-1.5, +0.5, -1.0);
        glVertex3f(-1.5, +0.5, -0.5);

        glVertex3f(-1.5, -0.5, -1.0);
        glVertex3f(-1.5, +0.5, -1.0);
        glVertex3f(-1.5, +0.5, -0.5);

        glVertex3f(-1.5, -1.5, -0.5);
        glVertex3f(-1.5, -0.5, -1.0);
        glVertex3f(-1.5, +0.5, -0.5);

        glVertex3f(-1.5, -1.5, -0.5);
        glVertex3f(-1.5, -0.5, -1.0);
        glVertex3f(-1.5, -1.5, -1.0);

        glVertex3f(-1.5, -0.5, -1.5);
        glVertex3f(-1.5, -0.5, -1.0);
        glVertex3f(-1.5, -1.5, -1.0);

        glVertex3f(-1.5, -0.5, -1.5);
        glVertex3f(-1.5, -1.5, -1.0);
        glVertex3f(-1.5, -1.5, -2.0);

        //faccia dx
        glColor3f(0.0, 0.0, 1.0);
        glVertex3f(+1.5, -1.5, -2.0);
        glVertex3f(+1.5, -0.5, -1.5);
        glVertex3f(+1.5, -0.5, -2.0);

        glVertex3f(+1.5, +0.5, -1.5);
        glVertex3f(+1.5, -0.5, -1.5);
        glVertex3f(+1.5, -0.5, -2.0);

        glVertex3f(+1.5, +0.5, -1.5);
        glVertex3f(+1.5, +1.5, -2.0);
        glVertex3f(+1.5, -0.5, -2.0);

        glVertex3f(+1.5, +0.5, -1.5);
        glVertex3f(+1.5, +1.5, -2.0);
        glVertex3f(+1.5, +1.5, -1.5);

        glVertex3f(+1.5, +0.5, -1.5);
        glVertex3f(+1.5, +0.5, -1.0);
        glVertex3f(+1.5, +1.5, -1.5);

        glVertex3f(+1.5, +1.5, -0.5);
        glVertex3f(+1.5, +0.5, -1.0);
        glVertex3f(+1.5, +1.5, -1.5);

        glVertex3f(+1.5, +1.5, -0.5);
        glVertex3f(+1.5, +0.5, -1.0);
        glVertex3f(+1.5, +0.5, -0.5);

        glVertex3f(+1.5, -0.5, -1.0);
        glVertex3f(+1.5, +0.5, -1.0);
        glVertex3f(+1.5, +0.5, -0.5);

        glVertex3f(+1.5, -1.5, -0.5);
        glVertex3f(+1.5, -0.5, -1.0);
        glVertex3f(+1.5, +0.5, -0.5);

        glVertex3f(+1.5, -1.5, -0.5);
        glVertex3f(+1.5, -0.5, -1.0);
        glVertex3f(+1.5, -1.5, -1.0);

        glVertex3f(+1.5, -0.5, -1.5);
        glVertex3f(+1.5, -0.5, -1.0);
        glVertex3f(+1.5, -1.5, -1.0);

        glVertex3f(+1.5, -0.5, -1.5);
        glVertex3f(+1.5, -1.5, -1.0);
        glVertex3f(+1.5, -1.5, -2.0);
        glEnd();
    #endif
    // restore model view settings
    glPopMatrix();
    glFinish();
}

// initialization function
void init (void)
{
    glClearColor (1.0, 1.0, 1.0, 0.0);
    #ifdef ARRAY
        glEnableClientState(GL_VERTEX_ARRAY);
        glEnableClientState(GL_COLOR_ARRAY);
        #ifdef VBO
            glGenBuffers(1, &buffer);
            // buffers[0] is for both vertices and colors
            // bind vertex buffer and reserve space.
            glBindBuffer(GL_ARRAY_BUFFER, buffer);
            glBufferData(GL_ARRAY_BUFFER, sizeof(cube) + sizeof(colorCube), NULL, GL_STATIC_DRAW);

            // Copy vertex coordinates data into first half of vertex buffer.
            glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(cube), cube);
            // Copy vertex color data into second half of vertex buffer.
            glBufferSubData(GL_ARRAY_BUFFER, sizeof(cube), sizeof(colorCube), colorCube);
            glVertexPointer(3, GL_FLOAT, 0, 0);
            glColorPointer(3, GL_FLOAT, 0, (GLvoid*)(sizeof(cube)));
        #else
            glVertexPointer(3, GL_FLOAT, 0, cube);
            glColorPointer(3, GL_FLOAT, 0, colorCube);
        #endif
    #endif

    // initialize viewing values
    glMatrixMode(GL_PROJECTION);

    // set viewing frustum
    glFrustum(-0.4, 0.2, -0.5, 0.1, 0.1, 4.1);

    // Turn on wireframe mode (only for debugging purposes)
    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    // initialize model view transforms
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    // Enable depth test
    glEnable(GL_DEPTH_TEST);
    // IMPORTANT: initialize the depth buffer
    // otherwise things are displayed erratically!
    glClear(GL_DEPTH_BUFFER_BIT);
    glDepthFunc(GL_LESS);
}

// Window size and mode
int main(int argc, char** argv)
{
    // pass potential input arguments to glutInit
    glutInit(&argc, argv);

    // set display mode
    // GLUT_SINGLE = single buffer window
    glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);

    glutInitWindowSize (400, 400);
    glutInitWindowPosition (100, 100);
    glutCreateWindow ("OpenGL Window");

    #ifdef VBO
        // Here we add the GLEW initalization
        GLenum err = glewInit();
        if (GLEW_OK != err) {
            printf("GLEW init failed: %s\n", glewGetErrorString(err));
            exit(1);
        } else {
            printf("GLEW init success\n");
        }
    #endif

    // Call initialization routinesx
    init();
    glutDisplayFunc(display);
    glutMainLoop();
    return 0;
}
