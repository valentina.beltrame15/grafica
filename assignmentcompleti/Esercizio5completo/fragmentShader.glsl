#version 430 core

#define SUN 0
#define DISC 1
#define MERCURY 2
#define VENUS 3
#define EARTH 4
#define MARS 5
#define JUPITER 6
#define SATURN 7
#define URANUS 8
#define NEPTUNE 9

// Material's properties
in vec4 frontAmbDiffExport, frontSpecExport, backAmbDiffExport, backSpecExport;
// texture coordinates
in vec2 texCoordsExport;

// samplers for the textures
uniform sampler2D merTex;
uniform sampler2D venTex;
uniform sampler2D earTex;
uniform sampler2D marTex;
uniform sampler2D jupTex;
uniform sampler2D satTex;
uniform sampler2D uraTex;
uniform sampler2D nepTex;

uniform uint object;

out vec4 colorsOut;

vec4 texColor;

void main(void)
{
    // check which object we are currently drawing
    if (object == SUN) texColor = texture(merTex, texCoordsExport);
    if (object == DISC) texColor = texture(merTex, texCoordsExport);
    if (object == MERCURY) texColor = texture(merTex, texCoordsExport);
    if (object == VENUS) texColor = texture(marTex, texCoordsExport);
    if (object == EARTH) texColor = texture(venTex, texCoordsExport);
    if (object == MARS) texColor = texture(earTex, texCoordsExport);
    if (object == JUPITER) texColor = texture(merTex, texCoordsExport);
    if (object == SATURN) texColor = texture(earTex, texCoordsExport);
    if (object == URANUS) texColor = texture(venTex, texCoordsExport);
    if (object == NEPTUNE) texColor = texture(venTex, texCoordsExport);

    // set color of the fragment
    colorsOut = gl_FrontFacing? (frontAmbDiffExport * texColor + frontSpecExport) :
                               (backAmbDiffExport * texColor + backSpecExport);
}
