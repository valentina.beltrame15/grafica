/**
 * -----------------------------------------------------------------------------
 * Computer Graphics - A.Y. 2021/2022
 * Teacher:  Antonino Casile
 * Students: Beltrame Valentina & Valerio Bongiovanni
 * -----------------------------------------------------------------------------
 * Assignment 05 -Ripetere l'assigment 4 usando gli shaders.

 * Solution: ho considerato il sole come un punto che emana luce
* NOTA- questo progetto è stato creato con visual studio code.
 *       Il comando per reanderlo operativo è :
 *       gcc main.c cylinder.c disc.c readBMP.c  shader.c -lglut -lGLU -lGL -lGLEW -lm
 *       mentre per avviarlo il comando è :
 *       ./a.out
 * ---------------------------------------------------------------------------------
 */


// standard includes
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <cglm/cglm.h>
#include <cglm/types-struct.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "shader.h"
#include "cylinder.h"
#include "disc.h"
#include "light.h"
#include "material.h"
#include "readBMP.h"

static enum object {SUN, DISC, MERCURY, VENUS, EARTH, MARS, JUPITER, SATURN, URANUS, NEPTUNE}; // VAO ids.
static enum buffer {SUN_VERTICES, SUN_INDICES, DISC_VERTICES, MER_VERTICES, MER_INDICES, VEN_VERTICES, VEN_INDICES,
                    EAR_VERTICES, EAR_INDICES, MAR_VERTICES, MAR_INDICES, JUP_VERTICES, JUP_INDICES,
                    SAT_VERTICES, SAT_INDICES, URA_VERTICES, URA_INDICES, NEP_VERTICES, NEP_INDICES}; // VBO ids.

// Globals.
static float Xangle = 0.785398, Yangle = 0.0, Zangle = 0.0; // Angles to rotate the cylinder.

// Light properties.
static const Light light0 =
{
    (vec4){0.0, 0.0, 0.0, 1.0},
    (vec4){1.0, 1.0, 1.0, 1.0},
    (vec4){1.0, 1.0, 1.0, 1.0},
    (vec4){0.0, 0.0, 1.0, 0.0}
};

// Global ambient.
static const vec4 globAmb =
{
    0.2, 0.2, 0.2, 1.0
};

// Front and back material properties.
static const Material canFandB =
{
    (vec4){1.0, 1.0, 1.0, 1.0},
    (vec4){1.0, 1.0, 1.0, 1.0},
    (vec4){1.0, 1.0, 1.0, 1.0},
    (vec4){0.0, 0.0, 0.0, 1.0},
    50.0f
};

// Cylinder data.
static Vertex cylVertices[(CYL_LONGS + 1) * (CYL_LATS + 1)];
static unsigned int cylIndices[CYL_LATS][2*(CYL_LONGS+1)];
static int cylCounts[CYL_LATS];
static void* cylOffsets[CYL_LATS];

// Disc data.
static Vertex discVertices[DISC_SEGS];

// Matrices
static mat4 modelViewMat = GLM_MAT4_IDENTITY_INIT;
static mat4 projMat = GLM_MAT4_IDENTITY_INIT;
static mat3 normalMat = GLM_MAT3_IDENTITY_INIT;

static unsigned int
programId,
vertexShaderId,
fragmentShaderId,
modelViewMatLoc,
normalMatLoc,
projMatLoc,
canLabelTexLoc,
canTopTexLoc,
merTexLoc,
venTexLoc,
earTexLoc,
marTexLoc,
jupTexLoc,
satTexLoc,
uraTexLoc,
nepTexLoc,
objectLoc,
buffer[19],
vao[10],
texture[4],
width,
height;

static BitMapFile *image[2]; // Local storage for bmp image data.

typedef struct {
    char    name[10];           // The name of the planet.
    GLdouble trajectoryRadius;   // Planet's circular trajectory radius.
    GLdouble revolutionStep;     // 1.0 / number of days to complete an orbit revolution.
    GLdouble currentStep;        // Current step of the interpolation for each orbit.
} Planet;

Planet P1, P2, P3, P4 ,P5 ,P6 ,P7 ,P8;

GLdouble x,y;

unsigned int interval = 500;

//calcola la rivoluzione dei pianeti intorno al sole
void revolution(int value){
    switch (value){
    case 1:
        P1.currentStep= P1.currentStep + P1.revolutionStep;
        
        glutPostRedisplay();
  
    break;

    case 2:
        P2.currentStep= P2.currentStep + P2.revolutionStep;

        
        glutPostRedisplay();
  
    break;

    case 3:
        P3.currentStep= P3.currentStep + P3.revolutionStep;
        
        glutPostRedisplay();
  
    break;

    case 4:
        P4.currentStep= P4.currentStep + P4.revolutionStep;
        
        glutPostRedisplay();
  
    break;


    case 5:
        P5.currentStep= P5.currentStep + P5.revolutionStep;
        
        glutPostRedisplay();
  
    break;

    case 6:
        P6.currentStep= P6.currentStep + P6.revolutionStep;
        
        glutPostRedisplay();
  
    break;

    case 7:
        P7.currentStep= P7.currentStep + P7.revolutionStep;
        
        glutPostRedisplay();
  
    break;

    case 8:
        P8.currentStep= P8.currentStep + P8.revolutionStep;
        
        glutPostRedisplay();
  
    break;

    }
    
    
}

// Initialization routine.
void setup(void)
{
    GLenum glErr;

      // ... it does not hurt to check that everything went OK
    if ((glErr = glGetError()) != 0)
    {
        printf("Errore = %d \n", glErr);
        exit(-1);
    }

    glClearColor(0.0, 0.0, 0.0, 0.0);
    glEnable(GL_DEPTH_TEST);

    //assegno i valori alla struttura

    strcpy(P1.name, "MERCURIO");
    P1.trajectoryRadius = 1.9;
    P1.revolutionStep = 0.114;
    P1.currentStep = 0.0;

     strcpy(P2.name, "VENERE");
    P2.trajectoryRadius = 2.9;
    P2.revolutionStep = 0.044;
    P2.currentStep = 0;

    strcpy(P3.name, "TERRA");
    P3.trajectoryRadius = 3.9;
    P3.revolutionStep = 0.027;
    P3.currentStep = 0;

    strcpy(P4.name, "MARTE");
    P4.trajectoryRadius = 4.9;
    P4.revolutionStep = 0.015;
    P4.currentStep = 0;

    strcpy(P5.name, "GIOVE");
    P5.trajectoryRadius = 6.1;
    P5.revolutionStep = 0.002;
    P5.currentStep = 0;


    strcpy(P6.name, "SATURNO");
    P6.trajectoryRadius = 7.5;
    P6.revolutionStep = 0.001;
    P6.currentStep = 0;

    strcpy(P7.name, "URANO");
    P7.trajectoryRadius = 8.7;
    P7.revolutionStep = 0.0003;
    P7.currentStep = 0;


    strcpy(P8.name, "NETTUNO");
    P8.trajectoryRadius = 9.7;
    P8.revolutionStep = 0.0002;
    P8.currentStep = 0;

    // Create shader program executable.
    vertexShaderId = setShader("vertex", "vertexShader.glsl");
    fragmentShaderId = setShader("fragment", "fragmentShader.glsl");
    programId = glCreateProgram();
    glAttachShader(programId, vertexShaderId);
    glAttachShader(programId, fragmentShaderId);
    glLinkProgram(programId);
    glUseProgram(programId);

    // Initialize cylinder and disc.
    
    fillDiscVertexArray(discVertices);

    // Create VAOs and VBOs...
    glGenVertexArrays(10, vao);
    glGenBuffers(19, buffer);
    

    //creo il sole
    fillCylinder(cylVertices, cylIndices, cylCounts, cylOffsets, 0);
    // ...and associate data with vertex shader.
    glBindVertexArray(vao[SUN]);
    glBindBuffer(GL_ARRAY_BUFFER, buffer[SUN_VERTICES]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cylVertices), cylVertices, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer[SUN_INDICES]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cylIndices), cylIndices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]), 0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]), (void*)sizeof(cylVertices[0].coords));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]),
                          (void*)(sizeof(cylVertices[0].coords)+sizeof(cylVertices[0].normal)));
    glEnableVertexAttribArray(2);
    

    // ...and associate data with vertex shader.
    glBindVertexArray(vao[DISC]);
    glBindBuffer(GL_ARRAY_BUFFER, buffer[DISC_VERTICES]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(discVertices), discVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(discVertices[0]), 0);
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(discVertices[0]), (void*)sizeof(discVertices[0].coords));
    glEnableVertexAttribArray(4);
    glVertexAttribPointer(5, 2, GL_FLOAT, GL_FALSE, sizeof(discVertices[0]),
                          (void*)(sizeof(discVertices[0].coords)+sizeof(discVertices[0].normal)));
    glEnableVertexAttribArray(5);


    //creo mercurio
    fillCylinder(cylVertices, cylIndices, cylCounts, cylOffsets, 1);
    // ...and associate data with vertex shader.
    glBindVertexArray(vao[MERCURY]);
    glBindBuffer(GL_ARRAY_BUFFER, buffer[MER_VERTICES]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cylVertices), cylVertices, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer[MER_INDICES]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cylIndices), cylIndices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]), 0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]), (void*)sizeof(cylVertices[0].coords));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]),
                          (void*)(sizeof(cylVertices[0].coords)+sizeof(cylVertices[0].normal)));
    glEnableVertexAttribArray(2);

    //creo venere
    fillCylinder(cylVertices, cylIndices, cylCounts, cylOffsets, 2);
    // ...and associate data with vertex shader.
    glBindVertexArray(vao[VENUS]);
    glBindBuffer(GL_ARRAY_BUFFER, buffer[VEN_VERTICES]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cylVertices), cylVertices, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer[VEN_INDICES]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cylIndices), cylIndices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]), 0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]), (void*)sizeof(cylVertices[0].coords));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]),
                          (void*)(sizeof(cylVertices[0].coords)+sizeof(cylVertices[0].normal)));
    glEnableVertexAttribArray(2);


    //creo la terra
    fillCylinder(cylVertices, cylIndices, cylCounts, cylOffsets, 3);
    // ...and associate data with vertex shader.
    glBindVertexArray(vao[EARTH]);
    glBindBuffer(GL_ARRAY_BUFFER, buffer[EAR_VERTICES]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cylVertices), cylVertices, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer[EAR_INDICES]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cylIndices), cylIndices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]), 0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]), (void*)sizeof(cylVertices[0].coords));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]),
                          (void*)(sizeof(cylVertices[0].coords)+sizeof(cylVertices[0].normal)));
    glEnableVertexAttribArray(2);


    //creo marte
    fillCylinder(cylVertices, cylIndices, cylCounts, cylOffsets, 4);
    // ...and associate data with vertex shader.
    glBindVertexArray(vao[MARS]);
    glBindBuffer(GL_ARRAY_BUFFER, buffer[MAR_VERTICES]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cylVertices), cylVertices, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer[MAR_INDICES]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cylIndices), cylIndices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]), 0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]), (void*)sizeof(cylVertices[0].coords));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]),
                          (void*)(sizeof(cylVertices[0].coords)+sizeof(cylVertices[0].normal)));
    glEnableVertexAttribArray(2);


    //creo giove
    fillCylinder(cylVertices, cylIndices, cylCounts, cylOffsets, 5);
    // ...and associate data with vertex shader.
    glBindVertexArray(vao[JUPITER]);
    glBindBuffer(GL_ARRAY_BUFFER, buffer[JUP_VERTICES]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cylVertices), cylVertices, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer[JUP_INDICES]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cylIndices), cylIndices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]), 0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]), (void*)sizeof(cylVertices[0].coords));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]),
                          (void*)(sizeof(cylVertices[0].coords)+sizeof(cylVertices[0].normal)));
    glEnableVertexAttribArray(2);


    //creo saturno
    fillCylinder(cylVertices, cylIndices, cylCounts, cylOffsets, 6);
    // ...and associate data with vertex shader.
    glBindVertexArray(vao[SATURN]);
    glBindBuffer(GL_ARRAY_BUFFER, buffer[SAT_VERTICES]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cylVertices), cylVertices, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer[SAT_INDICES]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cylIndices), cylIndices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]), 0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]), (void*)sizeof(cylVertices[0].coords));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]),
                          (void*)(sizeof(cylVertices[0].coords)+sizeof(cylVertices[0].normal)));
    glEnableVertexAttribArray(2);


    //creo urano
    fillCylinder(cylVertices, cylIndices, cylCounts, cylOffsets, 7);
    // ...and associate data with vertex shader.
    glBindVertexArray(vao[URANUS]);
    glBindBuffer(GL_ARRAY_BUFFER, buffer[URA_VERTICES]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cylVertices), cylVertices, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer[URA_INDICES]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cylIndices), cylIndices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]), 0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]), (void*)sizeof(cylVertices[0].coords));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]),
                          (void*)(sizeof(cylVertices[0].coords)+sizeof(cylVertices[0].normal)));
    glEnableVertexAttribArray(2);


    //creo nettuno
    fillCylinder(cylVertices, cylIndices, cylCounts, cylOffsets, 8);
    // ...and associate data with vertex shader.
    glBindVertexArray(vao[NEPTUNE]);
    glBindBuffer(GL_ARRAY_BUFFER, buffer[NEP_VERTICES]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cylVertices), cylVertices, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer[NEP_INDICES]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cylIndices), cylIndices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]), 0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]), (void*)sizeof(cylVertices[0].coords));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]),
                          (void*)(sizeof(cylVertices[0].coords)+sizeof(cylVertices[0].normal)));
    glEnableVertexAttribArray(2);

    // Obtain modelview matrix, projection matrix, normal matrix and object uniform locations.
    modelViewMatLoc = glGetUniformLocation(programId, "modelViewMat");
    projMatLoc = glGetUniformLocation(programId, "projMat");
    normalMatLoc = glGetUniformLocation(programId, "normalMat");
    objectLoc = glGetUniformLocation(programId, "object");

    // Obtain light property uniform locations and set values.
    glUniform4fv(glGetUniformLocation(programId, "light0.ambCols"), 1, &light0.ambCols[0]);
    glUniform4fv(glGetUniformLocation(programId, "light0.difCols"), 1, &light0.difCols[0]);
    glUniform4fv(glGetUniformLocation(programId, "light0.specCols"), 1, &light0.specCols[0]);
    glUniform4fv(glGetUniformLocation(programId, "light0.coords"), 1, &light0.coords[0]);

    // Obtain global ambient uniform location and set value.
    glUniform4fv(glGetUniformLocation(programId, "globAmb"), 1, &globAmb[0]);

    // Obtain material property uniform locations and set values.
    glUniform4fv(glGetUniformLocation(programId, "canFandB.ambRefl"), 1, &canFandB.ambRefl[0]);
    glUniform4fv(glGetUniformLocation(programId, "canFandB.difRefl"), 1, &canFandB.difRefl[0]);
    glUniform4fv(glGetUniformLocation(programId, "canFandB.specRefl"), 1, &canFandB.specRefl[0]);
    glUniform4fv(glGetUniformLocation(programId, "canFandB.emitCols"), 1, &canFandB.emitCols[0]);
    glUniform1f(glGetUniformLocation(programId, "canFandB.shininess"), canFandB.shininess);

    // Load the images.
    image[0] = readBMP("./grigio.bmp");
    image[1] = readBMP("./blu.bmp");
    image[2] = readBMP("./rosso.bmp");
    image[3] = readBMP("./beige.bmp");
    

    // Create texture ids.
    glGenTextures(4, texture);

    // Bind grigio image.
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture[0]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image[0]->sizeX, image[0]->sizeY, 0,
                 GL_RGBA, GL_UNSIGNED_BYTE, image[0]->data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    merTexLoc = glGetUniformLocation(programId, "merTex");
    glUniform1i(merTexLoc, 0);

    // Bind blu image.
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, texture[1]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image[1]->sizeX, image[1]->sizeY, 0,
                 GL_RGBA, GL_UNSIGNED_BYTE, image[1]->data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    venTexLoc = glGetUniformLocation(programId, "venTex");
    glUniform1i(venTexLoc, 1);

    // Bind rosso image.
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, texture[2]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image[2]->sizeX, image[2]->sizeY, 0,
                 GL_RGBA, GL_UNSIGNED_BYTE, image[2]->data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    earTexLoc = glGetUniformLocation(programId, "earTex");
    glUniform1i(earTexLoc, 2);

    // Bind beige image.
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, texture[3]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image[3]->sizeX, image[3]->sizeY, 0,
                 GL_RGBA, GL_UNSIGNED_BYTE, image[3]->data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    marTexLoc = glGetUniformLocation(programId, "marTex");
    glUniform1i(marTexLoc, 3);

    

    // ... it does not hurt to check that everything went OK
    if ((glErr = glGetError()) != 0)
    {
        printf("Errore = %d \n", glErr);
        exit(-1);
    }

}

// Drawing routine.
void display(void)
{
    mat3 TMP;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Calculate and update projection matrix.
    glm_perspective(30.0f, (float)width/(float)height, 1.0f, 50.0f, projMat);
    glUniformMatrix4fv(projMatLoc, 1, GL_FALSE, (GLfloat *) projMat);

    // Calculate and update modelview matrix.
    glm_mat4_identity(modelViewMat);
    glm_lookat((vec3){0.0, 0.0, 3.0}, (vec3){0.0, 0.0, 0.0},
        (vec3){0.0, 1.0, 0.0}, modelViewMat);

    glutTimerFunc(interval, revolution,0);

    glm_rotate(modelViewMat, Zangle, (vec3){0.0, 0.0, 1.0});
    glm_rotate(modelViewMat, Yangle, (vec3){0.0, 1.0, 0.0});
    glm_rotate(modelViewMat, Xangle, (vec3){1.0, 0.0, 0.0});
    glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)(modelViewMat));

    // Calculate and update normal matrix.
    glm_mat4_pick3(modelViewMat, TMP);
    glm_mat3_inv(TMP, normalMat);
    glm_mat3_transpose(normalMat);
    glUniformMatrix3fv(normalMatLoc, 1, GL_FALSE, (GLfloat *)normalMat);

    // creo il sole (sarà un punto)
    glUniform1ui(objectLoc, SUN);
    glBindVertexArray(vao[SUN]);
    glMultiDrawElements(GL_TRIANGLE_STRIP, cylCounts, GL_UNSIGNED_INT, (const void **)cylOffsets, CYL_LATS);
    glm_scale(modelViewMat, (vec3){1.0, -1.0, 1.0}); 

    glm_mat4_pick3(modelViewMat, TMP);
    glm_mat3_inv(TMP, normalMat);
    glm_mat3_transpose(normalMat);
    glUniformMatrix3fv(normalMatLoc, 1, GL_FALSE, (GLfloat *)normalMat);
    glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)modelViewMat);
    glMultiDrawElements(GL_TRIANGLE_STRIP, cylCounts, GL_UNSIGNED_INT, (const void **)cylOffsets, CYL_LATS);

    glm_mat4_pick3(modelViewMat, TMP);
    glm_mat3_inv(TMP, normalMat);
    glm_mat3_transpose(normalMat);
    glUniformMatrix3fv(normalMatLoc, 1, GL_FALSE, (GLfloat *)normalMat);

    //creo i vari pianeti
    glutTimerFunc(interval, revolution,1);
    x=0.5/2*sin(P1.currentStep)*P1.trajectoryRadius;
    y=0.5/2*cos(P1.currentStep)*P1.trajectoryRadius;

    glm_translate(modelViewMat, (vec3){x, y, 0.0});
    glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)modelViewMat);

    glUniform1ui(objectLoc, MERCURY);
    glBindVertexArray(vao[MERCURY]);
    glMultiDrawElements(GL_TRIANGLE_STRIP, cylCounts, GL_UNSIGNED_INT, (const void **)cylOffsets, CYL_LATS);
    glm_scale(modelViewMat, (vec3){1.0, -1.0, 1.0}); 
    glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)modelViewMat);
    glm_mat4_pick3(modelViewMat, TMP);
    glm_mat3_inv(TMP, normalMat);
    glm_mat3_transpose(normalMat);
    glUniformMatrix3fv(normalMatLoc, 1, GL_FALSE, (GLfloat *)normalMat);
    glMultiDrawElements(GL_TRIANGLE_STRIP, cylCounts, GL_UNSIGNED_INT, (const void **)cylOffsets, CYL_LATS);

    glm_scale(modelViewMat, (vec3){1.0, -1.0, 1.0});
    glm_translate(modelViewMat, (vec3){-x, -y, 0.0});
    glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)modelViewMat);

    glm_mat4_pick3(modelViewMat, TMP);
    glm_mat3_inv(TMP, normalMat);
    glm_mat3_transpose(normalMat);
    glUniformMatrix3fv(normalMatLoc, 1, GL_FALSE, (GLfloat *)normalMat);



    glutTimerFunc(interval, revolution,2);
    x=0.5/2*sin(P2.currentStep)*P2.trajectoryRadius;
    y=0.5/2*cos(P2.currentStep)*P2.trajectoryRadius;

    glm_translate(modelViewMat, (vec3){x, y, 0.0});
    glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)modelViewMat);

    glUniform1ui(objectLoc, VENUS);
    glBindVertexArray(vao[VENUS]);
    glMultiDrawElements(GL_TRIANGLE_STRIP, cylCounts, GL_UNSIGNED_INT, (const void **)cylOffsets, CYL_LATS);
    glm_scale(modelViewMat, (vec3){1.0, -1.0, 1.0}); 
    glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)modelViewMat);
    glm_mat4_pick3(modelViewMat, TMP);
    glm_mat3_inv(TMP, normalMat);
    glm_mat3_transpose(normalMat);
    glUniformMatrix3fv(normalMatLoc, 1, GL_FALSE, (GLfloat *)normalMat);
    glMultiDrawElements(GL_TRIANGLE_STRIP, cylCounts, GL_UNSIGNED_INT, (const void **)cylOffsets, CYL_LATS);

    glm_scale(modelViewMat, (vec3){1.0, -1.0, 1.0});
    glm_translate(modelViewMat, (vec3){-x, -y, 0.0});
    glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)modelViewMat);

    glm_mat4_pick3(modelViewMat, TMP);
    glm_mat3_inv(TMP, normalMat);
    glm_mat3_transpose(normalMat);
    glUniformMatrix3fv(normalMatLoc, 1, GL_FALSE, (GLfloat *)normalMat);



    glutTimerFunc(interval, revolution,3);
    x=0.5/2*sin(P3.currentStep)*P3.trajectoryRadius;
    y=0.5/2*cos(P3.currentStep)*P3.trajectoryRadius;

    glm_translate(modelViewMat, (vec3){x, y, 0.0});
    glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)modelViewMat);

    glUniform1ui(objectLoc, EARTH);
    glBindVertexArray(vao[EARTH]);
    glMultiDrawElements(GL_TRIANGLE_STRIP, cylCounts, GL_UNSIGNED_INT, (const void **)cylOffsets, CYL_LATS);
    glm_scale(modelViewMat, (vec3){1.0, -1.0, 1.0}); 
    glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)modelViewMat);
    glm_mat4_pick3(modelViewMat, TMP);
    glm_mat3_inv(TMP, normalMat);
    glm_mat3_transpose(normalMat);
    glUniformMatrix3fv(normalMatLoc, 1, GL_FALSE, (GLfloat *)normalMat);
    glMultiDrawElements(GL_TRIANGLE_STRIP, cylCounts, GL_UNSIGNED_INT, (const void **)cylOffsets, CYL_LATS);

    glm_scale(modelViewMat, (vec3){1.0, -1.0, 1.0});
    glm_translate(modelViewMat, (vec3){-x, -y, 0.0});
    glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)modelViewMat);

    glm_mat4_pick3(modelViewMat, TMP);
    glm_mat3_inv(TMP, normalMat);
    glm_mat3_transpose(normalMat);
    glUniformMatrix3fv(normalMatLoc, 1, GL_FALSE, (GLfloat *)normalMat);




    glutTimerFunc(interval, revolution,4);
    x=0.5/2*sin(P4.currentStep)*P4.trajectoryRadius;
    y=0.5/2*cos(P4.currentStep)*P4.trajectoryRadius;

    glm_translate(modelViewMat, (vec3){x, y, 0.0});
    glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)modelViewMat);

    glUniform1ui(objectLoc, MARS);
    glBindVertexArray(vao[MARS]);
    glMultiDrawElements(GL_TRIANGLE_STRIP, cylCounts, GL_UNSIGNED_INT, (const void **)cylOffsets, CYL_LATS);
    glm_scale(modelViewMat, (vec3){1.0, -1.0, 1.0}); 
    glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)modelViewMat);
    glm_mat4_pick3(modelViewMat, TMP);
    glm_mat3_inv(TMP, normalMat);
    glm_mat3_transpose(normalMat);
    glUniformMatrix3fv(normalMatLoc, 1, GL_FALSE, (GLfloat *)normalMat);
    glMultiDrawElements(GL_TRIANGLE_STRIP, cylCounts, GL_UNSIGNED_INT, (const void **)cylOffsets, CYL_LATS);

    glm_scale(modelViewMat, (vec3){1.0, -1.0, 1.0});
    glm_translate(modelViewMat, (vec3){-x, -y, 0.0});
    glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)modelViewMat);

    glm_mat4_pick3(modelViewMat, TMP);
    glm_mat3_inv(TMP, normalMat);
    glm_mat3_transpose(normalMat);
    glUniformMatrix3fv(normalMatLoc, 1, GL_FALSE, (GLfloat *)normalMat);


    
    glutTimerFunc(interval, revolution,5);
    x=0.5/2*sin(P5.currentStep)*P5.trajectoryRadius;
    y=0.5/2*cos(P5.currentStep)*P5.trajectoryRadius;

    glm_translate(modelViewMat, (vec3){x, y, 0.0});
    glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)modelViewMat);
    
    glUniform1ui(objectLoc, JUPITER);
    glBindVertexArray(vao[JUPITER]);
    glMultiDrawElements(GL_TRIANGLE_STRIP, cylCounts, GL_UNSIGNED_INT, (const void **)cylOffsets, CYL_LATS);
    glm_scale(modelViewMat, (vec3){1.0, -1.0, 1.0}); 
    glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)modelViewMat);
    glm_mat4_pick3(modelViewMat, TMP);
    glm_mat3_inv(TMP, normalMat);
    glm_mat3_transpose(normalMat);
    glUniformMatrix3fv(normalMatLoc, 1, GL_FALSE, (GLfloat *)normalMat);
    glMultiDrawElements(GL_TRIANGLE_STRIP, cylCounts, GL_UNSIGNED_INT, (const void **)cylOffsets, CYL_LATS);

    glm_scale(modelViewMat, (vec3){1.0, -1.0, 1.0});
    glm_translate(modelViewMat, (vec3){-x, -y, 0.0});
    glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)modelViewMat);

    glm_mat4_pick3(modelViewMat, TMP);
    glm_mat3_inv(TMP, normalMat);
    glm_mat3_transpose(normalMat);
    glUniformMatrix3fv(normalMatLoc, 1, GL_FALSE, (GLfloat *)normalMat);



    glutTimerFunc(interval, revolution,6);
    x=0.5/2*sin(P6.currentStep)*P6.trajectoryRadius;
    y=0.5/2*cos(P6.currentStep)*P6.trajectoryRadius;

    glm_translate(modelViewMat, (vec3){x, y, 0.0});
    glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)modelViewMat);

    glUniform1ui(objectLoc, SATURN);
    glBindVertexArray(vao[SATURN]);
    glMultiDrawElements(GL_TRIANGLE_STRIP, cylCounts, GL_UNSIGNED_INT, (const void **)cylOffsets, CYL_LATS);
    glm_scale(modelViewMat, (vec3){1.0, -1.0, 1.0}); 
    glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)modelViewMat);
    glm_mat4_pick3(modelViewMat, TMP);
    glm_mat3_inv(TMP, normalMat);
    glm_mat3_transpose(normalMat);
    glUniformMatrix3fv(normalMatLoc, 1, GL_FALSE, (GLfloat *)normalMat);
    glMultiDrawElements(GL_TRIANGLE_STRIP, cylCounts, GL_UNSIGNED_INT, (const void **)cylOffsets, CYL_LATS);

    glm_scale(modelViewMat, (vec3){1.0, -1.0, 1.0});
    glm_translate(modelViewMat, (vec3){-x, -y, 0.0});
    glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)modelViewMat);

    glm_mat4_pick3(modelViewMat, TMP);
    glm_mat3_inv(TMP, normalMat);
    glm_mat3_transpose(normalMat);
    glUniformMatrix3fv(normalMatLoc, 1, GL_FALSE, (GLfloat *)normalMat);



    glutTimerFunc(interval, revolution,7);
    x=0.5/2*sin(P7.currentStep)*P7.trajectoryRadius;
    y=0.5/2*cos(P7.currentStep)*P7.trajectoryRadius;

    glm_translate(modelViewMat, (vec3){x, y, 0.0});
    glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)modelViewMat);

    glUniform1ui(objectLoc, URANUS);
    glBindVertexArray(vao[URANUS]);
    glMultiDrawElements(GL_TRIANGLE_STRIP, cylCounts, GL_UNSIGNED_INT, (const void **)cylOffsets, CYL_LATS);
    glm_scale(modelViewMat, (vec3){1.0, -1.0, 1.0}); 
    glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)modelViewMat);
    glm_mat4_pick3(modelViewMat, TMP);
    glm_mat3_inv(TMP, normalMat);
    glm_mat3_transpose(normalMat);
    glUniformMatrix3fv(normalMatLoc, 1, GL_FALSE, (GLfloat *)normalMat);
    glMultiDrawElements(GL_TRIANGLE_STRIP, cylCounts, GL_UNSIGNED_INT, (const void **)cylOffsets, CYL_LATS);

    glm_scale(modelViewMat, (vec3){1.0, -1.0, 1.0});
    glm_translate(modelViewMat, (vec3){-x, -y, 0.0});
    glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)modelViewMat);

    glm_mat4_pick3(modelViewMat, TMP);
    glm_mat3_inv(TMP, normalMat);
    glm_mat3_transpose(normalMat);
    glUniformMatrix3fv(normalMatLoc, 1, GL_FALSE, (GLfloat *)normalMat);



    glutTimerFunc(interval, revolution,8);
    x=0.5/2*sin(P8.currentStep)*P8.trajectoryRadius;
    y=0.5/2*cos(P8.currentStep)*P8.trajectoryRadius;

    glm_translate(modelViewMat, (vec3){x, y, 0.0});
    glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)modelViewMat);

    glUniform1ui(objectLoc, NEPTUNE);
    glBindVertexArray(vao[NEPTUNE]);
    glMultiDrawElements(GL_TRIANGLE_STRIP, cylCounts, GL_UNSIGNED_INT, (const void **)cylOffsets, CYL_LATS);
    glm_scale(modelViewMat, (vec3){1.0, -1.0, 1.0}); 
    glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)modelViewMat);
    glm_mat4_pick3(modelViewMat, TMP);
    glm_mat3_inv(TMP, normalMat);
    glm_mat3_transpose(normalMat);
    glUniformMatrix3fv(normalMatLoc, 1, GL_FALSE, (GLfloat *)normalMat);
    glMultiDrawElements(GL_TRIANGLE_STRIP, cylCounts, GL_UNSIGNED_INT, (const void **)cylOffsets, CYL_LATS);

    glm_scale(modelViewMat, (vec3){1.0, -1.0, 1.0});
    glm_translate(modelViewMat, (vec3){-x, -y, 0.0});
    glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)modelViewMat);
  

    glutSwapBuffers();
}

// OpenGL window reshape routine.
void resize(int w, int h)
{
    glViewport(0, 0, w, h);
    width = w;
    height = h;
}




// Main routine.
int main(int argc, char **argv)
{
    GLenum glErr;


    glutInit(&argc, argv);
    glutInitContextVersion(4, 3);
    glutInitContextProfile(GLUT_CORE_PROFILE);
    glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);

    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
    glutInitWindowSize(2000, 2000);
    glutInitWindowPosition(100, 100);

    glutCreateWindow("Sistema solare con shaders");
    glutDisplayFunc(display);
    glutReshapeFunc(resize);
    glewInit();

    // ... it does not hurt to check that everything went OK
    if ((glErr = glGetError()) != 0)
    {
        printf("Errore = %d \n", glErr);
        exit(-1);
    }

    setup();
    glutMainLoop();

    return 0;
}
