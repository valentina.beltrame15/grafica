#include <GL/glew.h>
#include <GL/freeglut.h>
#include <cglm/cglm.h>

#include <cglm/types-struct.h>
#include <stdio.h>
#include <math.h>

#include "cylinder.h"


// Fill the vertex array with co-ordinates of the sample points.
void fillCylVertexArray(Vertex cylVertices[(CYL_LONGS + 1) * (CYL_LATS + 1)], int planet)
{
   int i, j, k;

   k = 0;

   switch (planet)
    {
    case 0:
        HEM_RADIUS=0.001;
    break;

    case 1:
        HEM_RADIUS=0.05;
    break;

    case 2:
        HEM_RADIUS=0.08;
    break;

    case 3:
        HEM_RADIUS=0.08;
    break;
    
    case 4:
        HEM_RADIUS=0.07;
    break;
    
    case 5:
        HEM_RADIUS=0.15;
    break;

    case 6:
        HEM_RADIUS=0.13;
    break;

    case 7:
        HEM_RADIUS=0.1;
    break;

    case 8:
        HEM_RADIUS=0.1;
    break;

    


    default:
        break;
    }

   for (j = 0; j <= CYL_LATS; j++)
      for (i = 0; i <= CYL_LONGS; i++)
      {
            cylVertices[k].coords.x = HEM_RADIUS * sin( (float)j/CYL_LATS * M_PI/2.0 ) *
                sin( 2.0 * (float)i/CYL_LONGS * M_PI );
            cylVertices[k].coords.y = HEM_RADIUS * cos( (float)j/CYL_LATS * M_PI/2.0 );
            cylVertices[k].coords.z = HEM_RADIUS * sin( (float)j/CYL_LATS * M_PI/2.0 ) *
                cos( 2.0 * (float)i/CYL_LONGS * M_PI );
            cylVertices[k].coords.w = 1.0;

		 cylVertices[k].normal.x = -sin( (2*(float)i/CYL_LONGS) * M_PI );
         cylVertices[k].normal.y = cos( (2*(float)i/CYL_LONGS) * M_PI );
		 cylVertices[k].normal.z = (float)j/CYL_LATS;
		 cylVertices[k].texCoords.x = (float)i/CYL_LONGS;
		 cylVertices[k].texCoords.y = (float)j/CYL_LATS;
		 k++;
	  }
}

// Fill the array of index arrays.
void fillCylIndices(unsigned int cylIndices[CYL_LATS][2*(CYL_LONGS+1)])
{
   int i, j;
   for(j = 0; j < CYL_LATS; j++)
   {
      for (i = 0; i <= CYL_LONGS; i++)
      {
	     cylIndices[j][2*i] = (j+1)*(CYL_LONGS + 1) + i;
	     cylIndices[j][2*i+1] = j*(CYL_LONGS + 1) + i;
      }
   }
}

// Fill the array of counts.
void fillCylCounts(int cylCounts[CYL_LATS])
{
   int j;
   for(j = 0; j < CYL_LATS; j++) cylCounts[j] = 2*(CYL_LONGS + 1);
}

// Fill the array of buffer offsets.
void fillCylOffsets(void* cylOffsets[CYL_LATS])
{
   int j;
   for(j = 0; j < CYL_LATS; j++) cylOffsets[j] = (GLvoid*)(2*(CYL_LONGS+1)*j*sizeof(unsigned int));
}

// Initialize the cylinder.
void fillCylinder(Vertex cylVertices[(CYL_LONGS + 1) * (CYL_LATS + 1)],
	         unsigned int cylIndices[CYL_LATS][2*(CYL_LONGS+1)],
			 int cylCounts[CYL_LATS],
			 void* cylOffsets[CYL_LATS], int planet)
{
   fillCylVertexArray(cylVertices, planet);
   fillCylIndices(cylIndices);
   fillCylCounts(cylCounts);
   fillCylOffsets(cylOffsets);
}