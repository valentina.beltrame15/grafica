#ifndef CYLINDER_H_INCLUDED
#define CYLINDER_H_INCLUDED

#include "vertex.h"

#define CYL_LONGS 30 // Number of longitudinal slices.
#define CYL_LATS 10 // Number of latitudinal slices.
#define HEM_LONGS 10 // Number of longitudinal slices.
#define HEM_LATS 5 // Number of latitudinal slices.
#define HEM_COLORS 0.0, 1.0, 1.0, 1.0 // Hemisphere colors.
float HEM_RADIUS;

void fillCylVertexArray(Vertex cylVertices[(CYL_LONGS + 1) * (CYL_LATS + 1)], int planet);
void fillCylIndices(unsigned int cylIndices[CYL_LATS][2*(CYL_LONGS+1)]);
void fillCylCounts(int cylCounts[CYL_LATS]);
void fillCylOffsets(void* cylOffsets[CYL_LATS]);

void fillCylinder(Vertex cylVertices[(CYL_LONGS + 1) * (CYL_LATS + 1)],
	         unsigned int cylIndices[CYL_LATS][2*(CYL_LONGS+1)],
			 int cylCounts[CYL_LATS],
			 void* cylOffsets[CYL_LATS], int planet);

#endif
