
/**
 * -----------------------------------------------------------------------------
 * Computer Graphics - A.Y. 2021/2022
 * Teacher:  Antonino Casile
 * Students: Beltrame Valentina & Bongiovanni Valerio
 * -----------------------------------------------------------------------------
 * PROGETTO- Creare una dama in 3d alternativa 
 *           Per maggiori info leggere il file REGOLE_LEGGERE_ATTENTAMENTE.txt
 * 
 * NOTA- questo progetto è stato creato con visual studio code.
 *       Il comando per reanderlo operativo è :
 *       gcc main.c cylinder.c disc.c readBMP.c  shader.c -lglut -lGLU -lGL -lGLEW -lm
 *       mentre per avviarlo il comando è :
 *       ./a.out
 * ---------------------------------------------------------------------------------
 */

// standard includes
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <cglm/cglm.h>
#include <cglm/types-struct.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "shader.h"
#include "cylinder.h"
#include "disc.h"
#include "light.h"
#include "material.h"
#include "readBMP.h"

//creiamo i vao e i vbo per tutti gli oggetti che compongono il progetto
static enum object {CYLINDER, BOARD1, BOARD2, PEDBIABASE, PEDBIACIL, PEDNERCIL,PEDROSCIL,
                    PEDBLUCIL, PEDVIOCIL,PEDSELCIL}; // VAO ids.
static enum buffer {CYL_VERTICES, CYL_INDICES, BRD1_VERTICES, BRD2_VERTICES, PDB_VERTICES,
                     PDC_VERTICES, PDN_VERTICES, PDR_VERTICES, PDBL_VERTICES,PDV_VERTICES,
                     PDS_VERTICES}; // VBO ids.

// Globals.
// angoli che permettono l'inclinazione della tastiera
static float Xangle = 0.523599, Yangle = 0.0, Zangle = 0.0;

// check contiene le posizioni attuali delle pedine
int check[8][8]={
    {1,0,1,0,1,0,1,0},
    {0,1,0,1,0,1,0,1},
    {1,0,1,0,1,0,1,0},
    {0,3,0,3,0,3,0,3},
    {3,0,3,0,3,0,3,0},
    {0,2,0,2,0,2,0,2},
    {2,0,2,0,2,0,2,0},
    {0,2,0,2,0,2,0,2},

};

// inizio permette di resettare la scacchiera
int inizio[8][8]={
    {1,0,1,0,1,0,1,0},
    {0,1,0,1,0,1,0,1},
    {1,0,1,0,1,0,1,0},
    {0,3,0,3,0,3,0,3},
    {3,0,3,0,3,0,3,0},
    {0,2,0,2,0,2,0,2},
    {2,0,2,0,2,0,2,0},
    {0,2,0,2,0,2,0,2},

};

// Light properties.
static const Light light0 =
{
    (vec4){0.0, 0.0, 0.0, 1.0},
    (vec4){1.0, 1.0, 1.0, 1.0},
    (vec4){0.2, 0.2, 0.2, 1.0},
    (vec4){4.0, -2.0, 5.0, 0.0}
};

// Global ambient.
static const vec4 globAmb =
{
    0.2, 0.2, 0.2, 1.0
};

// Front and back material properties.
static const Material canFandB =
{
    (vec4){1.0, 1.0, 1.0, 1.0},
    (vec4){1.0, 1.0, 1.0, 1.0},
    (vec4){1.0, 1.0, 1.0, 1.0},
    (vec4){0.0, 0.0, 0.0, 1.0},
    50.0f
};

// Cylinder data. utilizzati per creare le pedine
static Vertex cylVertices[(CYL_LONGS + 1) * (CYL_LATS + 1)];
static unsigned int cylIndices[CYL_LATS][2*(CYL_LONGS+1)];
static int cylCounts[CYL_LATS];
static void* cylOffsets[CYL_LATS];

// Disc data. utilizzati per creare la scacchiera e la base delle pedine
static Vertex discVertices[DISC_SEGS];

// Matrices
static mat4 modelViewMat = GLM_MAT4_IDENTITY_INIT;
static mat4 projMat = GLM_MAT4_IDENTITY_INIT;
static mat3 normalMat = GLM_MAT3_IDENTITY_INIT;

static unsigned int
programId,
vertexShaderId,
fragmentShaderId,
modelViewMatLoc,
normalMatLoc,
projMatLoc,
marTexLoc,
beiTexLoc,
biaTexLoc,
nerTexLoc,
rosTexLoc,
verTexLoc,
bluTexLoc,
vioTexLoc,
objectLoc,
buffer[11],
vao[10],
texture[8],
width,
height;

//started inidica se il game è iniziato
bool started;

// selected viene utilizzato per selezionare la pedina 
// 0 non selezionata e quindi ancora in fase di scelta pedina
// 1 pedina selezionata 
bool selected;

// riga e col hanno un valore che varia da 1 a 8 e riga-1 e col-1 indicano la posizione della pedina attuale
// in check
int riga=0;
int col=0;

//turno indica quali pedine sono selezionabili
int turno=1;

static BitMapFile *image[4]; // Local storage for bmp image data.

// Initialization routine.
void setup(void)
{
    GLenum glErr;

      // ... it does not hurt to check that everything went OK
    if ((glErr = glGetError()) != 0)
    {
        printf("Errore = %d \n", glErr);
        exit(-1);
    }

    glClearColor(1.0, 1.0, 1.0, 0.0);
    glEnable(GL_DEPTH_TEST);

    // Create shader program executable.
    vertexShaderId = setShader("vertex", "vertexShader.glsl");
    fragmentShaderId = setShader("fragment", "fragmentShader.glsl");
    programId = glCreateProgram();
    glAttachShader(programId, vertexShaderId);
    glAttachShader(programId, fragmentShaderId);
    glLinkProgram(programId);
    glUseProgram(programId);


    
    

    // Create VAOs and VBOs...
    glGenVertexArrays(10, vao);
    glGenBuffers(11, buffer);

    // ...and associate data with vertex shader.
    fillCylinder(cylVertices, cylIndices, cylCounts, cylOffsets);
    glBindVertexArray(vao[CYLINDER]);
    glBindBuffer(GL_ARRAY_BUFFER, buffer[CYL_VERTICES]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cylVertices), cylVertices, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer[CYL_INDICES]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cylIndices), cylIndices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]), 0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]), (void*)sizeof(cylVertices[0].coords));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]),
                          (void*)(sizeof(cylVertices[0].coords)+sizeof(cylVertices[0].normal)));
    glEnableVertexAttribArray(2);


    //creiamo l'oggetto per le caselle in legno chiaro della scacchiera

    fillDiscVertexArray(discVertices, 1);
    // ...and associate data with vertex shader.
    glBindVertexArray(vao[BOARD1]);
    glBindBuffer(GL_ARRAY_BUFFER, buffer[BRD1_VERTICES]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(discVertices), discVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(discVertices[0]), 0);
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(discVertices[0]), (void*)sizeof(discVertices[0].coords));
    glEnableVertexAttribArray(4);
    glVertexAttribPointer(5, 2, GL_FLOAT, GL_FALSE, sizeof(discVertices[0]),
                          (void*)(sizeof(discVertices[0].coords)+sizeof(discVertices[0].normal)));
    glEnableVertexAttribArray(5);


    //creiamo l'oggetto per le caselle in legno scuro della scacchiera
    fillDiscVertexArray(discVertices, 1);
    glBindVertexArray(vao[BOARD2]);
    glBindBuffer(GL_ARRAY_BUFFER, buffer[BRD2_VERTICES]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(discVertices), discVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(discVertices[0]), 0);
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(discVertices[0]), (void*)sizeof(discVertices[0].coords));
    glEnableVertexAttribArray(4);
    glVertexAttribPointer(5, 2, GL_FLOAT, GL_FALSE, sizeof(discVertices[0]),
                          (void*)(sizeof(discVertices[0].coords)+sizeof(discVertices[0].normal)));
    glEnableVertexAttribArray(5);

    fillDiscVertexArray(discVertices, 2);
    glBindVertexArray(vao[PEDBIABASE]);
    glBindBuffer(GL_ARRAY_BUFFER, buffer[PDB_VERTICES]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(discVertices), discVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(discVertices[0]), 0);
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(discVertices[0]), (void*)sizeof(discVertices[0].coords));
    glEnableVertexAttribArray(4);
    glVertexAttribPointer(5, 2, GL_FLOAT, GL_FALSE, sizeof(discVertices[0]),
                          (void*)(sizeof(discVertices[0].coords)+sizeof(discVertices[0].normal)));
    glEnableVertexAttribArray(5);

    //creiamo l'oggetto per la pedina bianca
    fillCylinder(cylVertices, cylIndices, cylCounts, cylOffsets);
    glBindVertexArray(vao[PEDBIACIL]);
    glBindBuffer(GL_ARRAY_BUFFER, buffer[PDC_VERTICES]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cylVertices), cylVertices, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer[CYL_INDICES]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cylIndices), cylIndices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]), 0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]), (void*)sizeof(cylVertices[0].coords));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]),
                          (void*)(sizeof(cylVertices[0].coords)+sizeof(cylVertices[0].normal)));
    glEnableVertexAttribArray(2);


    //creiamo l'oggetto per la pedina nera
    fillCylinder(cylVertices, cylIndices, cylCounts, cylOffsets);
    glBindVertexArray(vao[PEDNERCIL]);
    glBindBuffer(GL_ARRAY_BUFFER, buffer[PDN_VERTICES]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cylVertices), cylVertices, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer[CYL_INDICES]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cylIndices), cylIndices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]), 0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]), (void*)sizeof(cylVertices[0].coords));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]),
                          (void*)(sizeof(cylVertices[0].coords)+sizeof(cylVertices[0].normal)));
    glEnableVertexAttribArray(2);


    //creiamo l'oggetto per la pedina non selezionata
    fillCylinder(cylVertices, cylIndices, cylCounts, cylOffsets);
    glBindVertexArray(vao[PEDROSCIL]);
    glBindBuffer(GL_ARRAY_BUFFER, buffer[PDR_VERTICES]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cylVertices), cylVertices, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer[CYL_INDICES]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cylIndices), cylIndices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]), 0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]), (void*)sizeof(cylVertices[0].coords));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]),
                          (void*)(sizeof(cylVertices[0].coords)+sizeof(cylVertices[0].normal)));
    glEnableVertexAttribArray(2);


    //creiamo l'oggetto per la dama delle pedine nere
    fillCylinder(cylVertices, cylIndices, cylCounts, cylOffsets);
    glBindVertexArray(vao[PEDBLUCIL]);
    glBindBuffer(GL_ARRAY_BUFFER, buffer[PDBL_VERTICES]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cylVertices), cylVertices, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer[CYL_INDICES]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cylIndices), cylIndices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]), 0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]), (void*)sizeof(cylVertices[0].coords));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]),
                          (void*)(sizeof(cylVertices[0].coords)+sizeof(cylVertices[0].normal)));
    glEnableVertexAttribArray(2);


    //creiamo l'oggetto per la dama delle pedine bianche
    fillCylinder(cylVertices, cylIndices, cylCounts, cylOffsets);
    glBindVertexArray(vao[PEDVIOCIL]);
    glBindBuffer(GL_ARRAY_BUFFER, buffer[PDV_VERTICES]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cylVertices), cylVertices, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer[CYL_INDICES]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cylIndices), cylIndices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]), 0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]), (void*)sizeof(cylVertices[0].coords));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]),
                          (void*)(sizeof(cylVertices[0].coords)+sizeof(cylVertices[0].normal)));
    glEnableVertexAttribArray(2);


    //creiamo l'oggetto per la pedina selezionata
    fillCylinder(cylVertices, cylIndices, cylCounts, cylOffsets);
    glBindVertexArray(vao[PEDSELCIL]);
    glBindBuffer(GL_ARRAY_BUFFER, buffer[PDS_VERTICES]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cylVertices), cylVertices, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer[CYL_INDICES]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cylIndices), cylIndices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]), 0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]), (void*)sizeof(cylVertices[0].coords));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(cylVertices[0]),
                          (void*)(sizeof(cylVertices[0].coords)+sizeof(cylVertices[0].normal)));
    glEnableVertexAttribArray(2);


    // Obtain modelview matrix, projection matrix, normal matrix and object uniform locations.
    modelViewMatLoc = glGetUniformLocation(programId, "modelViewMat");
    projMatLoc = glGetUniformLocation(programId, "projMat");
    normalMatLoc = glGetUniformLocation(programId, "normalMat");
    objectLoc = glGetUniformLocation(programId, "object");

    // Obtain light property uniform locations and set values.
    glUniform4fv(glGetUniformLocation(programId, "light0.ambCols"), 1, &light0.ambCols[0]);
    glUniform4fv(glGetUniformLocation(programId, "light0.difCols"), 1, &light0.difCols[0]);
    glUniform4fv(glGetUniformLocation(programId, "light0.specCols"), 1, &light0.specCols[0]);
    glUniform4fv(glGetUniformLocation(programId, "light0.coords"), 1, &light0.coords[0]);

    // Obtain global ambient uniform location and set value.
    glUniform4fv(glGetUniformLocation(programId, "globAmb"), 1, &globAmb[0]);

    // Obtain material property uniform locations and set values.
    glUniform4fv(glGetUniformLocation(programId, "canFandB.ambRefl"), 1, &canFandB.ambRefl[0]);
    glUniform4fv(glGetUniformLocation(programId, "canFandB.difRefl"), 1, &canFandB.difRefl[0]);
    glUniform4fv(glGetUniformLocation(programId, "canFandB.specRefl"), 1, &canFandB.specRefl[0]);
    glUniform4fv(glGetUniformLocation(programId, "canFandB.emitCols"), 1, &canFandB.emitCols[0]);
    glUniform1f(glGetUniformLocation(programId, "canFandB.shininess"), canFandB.shininess);

    // Load the images.
    image[0] = readBMP("./marrone.bmp");
    image[1] = readBMP("./beige.bmp");
    image[2] = readBMP("./bianconuovo.bmp");
    image[3] = readBMP("./nero.bmp");
    image[4] = readBMP("./rosso.bmp");
    image[5] = readBMP("./blu.bmp");
    image[6] = readBMP("./viola.bmp");
   

    // Create texture ids.
    glGenTextures(8, texture);

    // Bind marrone image
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture[0]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image[0]->sizeX, image[0]->sizeY, 0,
                 GL_RGBA, GL_UNSIGNED_BYTE, image[0]->data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    marTexLoc = glGetUniformLocation(programId, "marTex");
    glUniform1i(marTexLoc, 0);

    // Bind beige image.
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, texture[1]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image[1]->sizeX, image[1]->sizeY, 0,
                 GL_RGBA, GL_UNSIGNED_BYTE, image[1]->data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    beiTexLoc = glGetUniformLocation(programId, "beiTex");
    glUniform1i(beiTexLoc, 1);

    // Bind bianconuovo image.
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, texture[2]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image[2]->sizeX, image[2]->sizeY, 0,
                 GL_RGBA, GL_UNSIGNED_BYTE, image[2]->data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    biaTexLoc = glGetUniformLocation(programId, "biaTex");
    glUniform1i(biaTexLoc, 2);

    // Bind nero image.
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, texture[3]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image[3]->sizeX, image[3]->sizeY, 0,
                 GL_RGBA, GL_UNSIGNED_BYTE, image[3]->data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    nerTexLoc = glGetUniformLocation(programId, "nerTex");
    glUniform1i(nerTexLoc, 3);

    // Bind rosso image.
    glActiveTexture(GL_TEXTURE4);
    glBindTexture(GL_TEXTURE_2D, texture[4]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image[4]->sizeX, image[4]->sizeY, 0,
                 GL_RGBA, GL_UNSIGNED_BYTE, image[4]->data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    rosTexLoc = glGetUniformLocation(programId, "rosTex");
    glUniform1i(rosTexLoc, 4);

    // Bind blu image.
    glActiveTexture(GL_TEXTURE5);
    glBindTexture(GL_TEXTURE_2D, texture[5]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image[5]->sizeX, image[5]->sizeY, 0,
                 GL_RGBA, GL_UNSIGNED_BYTE, image[5]->data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    bluTexLoc = glGetUniformLocation(programId, "bluTex");
    glUniform1i(bluTexLoc, 5);

    // Bind viola image.
    glActiveTexture(GL_TEXTURE6);
    glBindTexture(GL_TEXTURE_2D, texture[6]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image[6]->sizeX, image[6]->sizeY, 0,
                 GL_RGBA, GL_UNSIGNED_BYTE, image[6]->data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    vioTexLoc = glGetUniformLocation(programId, "vioTex");
    glUniform1i(vioTexLoc, 6);

    

   

    // ... it does not hurt to check that everything went OK
    if ((glErr = glGetError()) != 0)
    {
        printf("Errore = %d \n", glErr);
        exit(-1);
    }

}

// Drawing routine.
void display(void)
{
    mat3 TMP;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Calculate and update projection matrix.
    glm_perspective(30.0f, (float)width/(float)height, 1.0f, 50.0f, projMat);
    glUniformMatrix4fv(projMatLoc, 1, GL_FALSE, (GLfloat *) projMat);

    // Calculate and update modelview matrix.
    glm_mat4_identity(modelViewMat);
    glm_lookat((vec3){0.0, 0.0, 3.0}, (vec3){0.0, 0.0, 0.0},
        (vec3){0.0, 1.0, 0.0}, modelViewMat);
    glm_rotate(modelViewMat, Zangle, (vec3){0.0, 0.0, 1.0});
    glm_rotate(modelViewMat, Yangle, (vec3){0.0, 1.0, 0.0});
    glm_rotate(modelViewMat, Xangle, (vec3){1.0, 0.0, 0.0});
    glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)(modelViewMat));

    // Calculate and update normal matrix.
    glm_mat4_pick3(modelViewMat, TMP);
    glm_mat3_inv(TMP, normalMat);
    glm_mat3_transpose(normalMat);
    glUniformMatrix3fv(normalMatLoc, 1, GL_FALSE, (GLfloat *)normalMat);


    glm_translate(modelViewMat, (vec3){2.0,1.0,0.0});
    glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)(modelViewMat));
    
    //effettuiamo un ciclo for per 8 righe
    for(int i=0;i<8;i++){
        if(i>0){
            //creiamo la riga successiva
            glm_translate(modelViewMat, (vec3){4.0,-0.5,0.0});
        }
        //effettuiamo un ciclo for per le 8 colonne
        for(int j=0; j<8; j++){
            //nel caso la riga sia pari le pedine partono dalla seconda colonna
            if(i%2==0){
                if(j%2==0){
                    // se in check abbiamo un valore pari a 3 abbiamo una casella vuota quindi senza pedina 
                    if(check[i][j]!=3){

                        glm_translate(modelViewMat, (vec3){-0.27,0.25,0.0});
                        glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)(modelViewMat));

                        //se in check abbiamo il valore 1 allora inidichiamo la pedina bianca
                        if(check[i][j]==1){
                            //controlliamo se una pedina sta per essere selezionata o è selezionata:
                            // nel primo caso la pedina sarà rossa nel secondo saraà verde
                            if(i==riga-1 && j==col-1 && selected==0){
                                glUniform1ui(objectLoc, PEDROSCIL);
                                glBindVertexArray(vao[PEDROSCIL]);
                                glMultiDrawElements(GL_TRIANGLE_FAN, cylCounts, GL_UNSIGNED_INT,(const void **)cylOffsets, CYL_LATS);
                            }else if(i==riga-1 && j==col-1 && selected==1){
                                glUniform1ui(objectLoc, PEDSELCIL);
                                glBindVertexArray(vao[PEDSELCIL]);
                                glMultiDrawElements(GL_TRIANGLE_FAN, cylCounts, GL_UNSIGNED_INT,(const void **)cylOffsets, CYL_LATS);
                            }
                            else
                            //nel caso il gioco non sia ancora iniziato o la pedina non sta per essere selezionata
                            //creaiamo la semplice pedina bianca
                            {
                                glUniform1ui(objectLoc, PEDBIACIL);
                                glBindVertexArray(vao[PEDBIACIL]);
                                glMultiDrawElements(GL_TRIANGLE_FAN, cylCounts, GL_UNSIGNED_INT,(const void **)cylOffsets, CYL_LATS);
                            }
                        }else 
                            // con il valore 2 in check indichiamo la pedina nera
                            if(check[i][j]==2) {
                            
                            //per la selezione e non, funziona come con le pedine bianche
                            if(i==riga-1 && j==col-1 && selected==0){
                                glUniform1ui(objectLoc, PEDROSCIL);
                                glBindVertexArray(vao[PEDROSCIL]);
                                glMultiDrawElements(GL_TRIANGLE_FAN, cylCounts, GL_UNSIGNED_INT,(const void **)cylOffsets, CYL_LATS);
                            }else if(i==riga-1 && j==col-1 && selected==1){
                                glUniform1ui(objectLoc, PEDSELCIL);
                                glBindVertexArray(vao[PEDSELCIL]);
                                glMultiDrawElements(GL_TRIANGLE_FAN, cylCounts, GL_UNSIGNED_INT,(const void **)cylOffsets, CYL_LATS);
                            }
                            else
                            {
                                glUniform1ui(objectLoc, PEDNERCIL);
                                glBindVertexArray(vao[PEDNERCIL]);
                                glMultiDrawElements(GL_TRIANGLE_FAN, cylCounts, GL_UNSIGNED_INT,(const void **)cylOffsets, CYL_LATS);
                            }
                        }else 
                        // con il valore 4 in check indichiamo la dama delle pedine viola (indicate con il colore viola)                        
                        if(check[i][j]==4){
                            //per la selezione e non, funzionano come le pedine bianche
                             if(i==riga-1 && j==col-1 && selected==0){
                                glUniform1ui(objectLoc, PEDROSCIL);
                                glBindVertexArray(vao[PEDROSCIL]);
                                glMultiDrawElements(GL_TRIANGLE_FAN, cylCounts, GL_UNSIGNED_INT,(const void **)cylOffsets, CYL_LATS);
                            }else if(i==riga-1 && j==col-1 && selected==1){
                                glUniform1ui(objectLoc, PEDSELCIL);
                                glBindVertexArray(vao[PEDSELCIL]);
                                glMultiDrawElements(GL_TRIANGLE_FAN, cylCounts, GL_UNSIGNED_INT,(const void **)cylOffsets, CYL_LATS);
                            }
                            else
                            {
                                glUniform1ui(objectLoc, PEDVIOCIL);
                                glBindVertexArray(vao[PEDVIOCIL]);
                                glMultiDrawElements(GL_TRIANGLE_FAN, cylCounts, GL_UNSIGNED_INT,(const void **)cylOffsets, CYL_LATS);
                            }
                        } else 
                        //con il valore 5 in check indichiamo la dama del nero (indicate con il colore blu)
                        if(check[i][j]==5){
                            //per la selezione e non, funzionano come le pedine bianche
                             if(i==riga-1 && j==col-1 && selected==0){
                                glUniform1ui(objectLoc, PEDROSCIL);
                                glBindVertexArray(vao[PEDROSCIL]);
                                glMultiDrawElements(GL_TRIANGLE_FAN, cylCounts, GL_UNSIGNED_INT,(const void **)cylOffsets, CYL_LATS);
                            }else if(i==riga-1 && j==col-1 && selected==1){
                                glUniform1ui(objectLoc, PEDSELCIL);
                                glBindVertexArray(vao[PEDSELCIL]);
                                glMultiDrawElements(GL_TRIANGLE_FAN, cylCounts, GL_UNSIGNED_INT,(const void **)cylOffsets, CYL_LATS);
                            }
                            else
                            {
                                glUniform1ui(objectLoc, PEDBLUCIL);
                                glBindVertexArray(vao[PEDBLUCIL]);
                                glMultiDrawElements(GL_TRIANGLE_FAN, cylCounts, GL_UNSIGNED_INT,(const void **)cylOffsets, CYL_LATS);
                            }
                        }        
                        
                        //per ogni pedina creata, aggiungiamo una base 
                        glm_translate(modelViewMat, (vec3){0.0,0.0,0.0});
                        glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)(modelViewMat));
                        glUniform1ui(objectLoc, PEDBIABASE);
                        glBindVertexArray(vao[PEDBIABASE]);
                        glDrawArrays(GL_TRIANGLE_FAN, 0, DISC_SEGS);
                        
                        //infine aggiungiamo la casella legno chiaro della scacchiera
                        glm_translate(modelViewMat, (vec3){-0.23,-0.25,0.0});
                        glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)(modelViewMat));
                        glUniform1ui(objectLoc, BOARD1);
                        glBindVertexArray(vao[BOARD1]);
                        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
                    }else{
                        //nel caso in check troviamo 3 creiamo solo la casella legno chiaro
                        glm_translate(modelViewMat, (vec3){-0.5,0.0,0.0});
                        glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)(modelViewMat));
                        glUniform1ui(objectLoc, BOARD1);
                        glBindVertexArray(vao[BOARD1]);
                        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
                    }
                    

                    
                    
                }else{

                    //creaiamo le caselle legno scuro della scacchiera
                    glm_translate(modelViewMat, (vec3){-0.5,0.0,0.0});

                    glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)(modelViewMat));
                    glUniform1ui(objectLoc, BOARD2);
                    glBindVertexArray(vao[BOARD2]);
                    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
                }
                   
            }
            //per le righe dispari
            if(i%2==1){
                //nelle colonne pari avremmo solo le caselle legno scuro della scacchiera
                if(j%2==0){
                    glm_translate(modelViewMat, (vec3){-0.5,0.0,0.0});
                    glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)(modelViewMat));
                    glUniform1ui(objectLoc, BOARD2);
                    glBindVertexArray(vao[BOARD2]);
                    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
                    
                }else{
                    //funzionano esattamente come per le righe pari
                    if(check[i][j]!=3){
                        glm_translate(modelViewMat, (vec3){-0.27,0.25,0.0});
                        glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)(modelViewMat));
                
            
                       if(check[i][j]==1){
                            if(i==riga-1 && j==col-1 && selected==0){
                                glUniform1ui(objectLoc, PEDROSCIL);
                                glBindVertexArray(vao[PEDROSCIL]);
                                glMultiDrawElements(GL_TRIANGLE_FAN, cylCounts, GL_UNSIGNED_INT,(const void **)cylOffsets, CYL_LATS);
                            }else if(i==riga-1 && j==col-1 && selected==1){
                                glUniform1ui(objectLoc, PEDSELCIL);
                                glBindVertexArray(vao[PEDSELCIL]);
                                glMultiDrawElements(GL_TRIANGLE_FAN, cylCounts, GL_UNSIGNED_INT,(const void **)cylOffsets, CYL_LATS);
                            }
                            else
                            {
                                glUniform1ui(objectLoc, PEDBIACIL);
                                glBindVertexArray(vao[PEDBIACIL]);
                                glMultiDrawElements(GL_TRIANGLE_FAN, cylCounts, GL_UNSIGNED_INT,(const void **)cylOffsets, CYL_LATS);
                            }
                        }else if(check[i][j]==2) {
                            if(i==riga-1 && j==col-1 && selected==0){
                                glUniform1ui(objectLoc, PEDROSCIL);
                                glBindVertexArray(vao[PEDROSCIL]);
                                glMultiDrawElements(GL_TRIANGLE_FAN, cylCounts, GL_UNSIGNED_INT,(const void **)cylOffsets, CYL_LATS);
                            }else if(i==riga-1 && j==col-1 && selected==1){
                                glUniform1ui(objectLoc, PEDSELCIL);
                                glBindVertexArray(vao[PEDSELCIL]);
                                glMultiDrawElements(GL_TRIANGLE_FAN, cylCounts, GL_UNSIGNED_INT,(const void **)cylOffsets, CYL_LATS);
                            }
                            else
                            {
                                glUniform1ui(objectLoc, PEDNERCIL);
                                glBindVertexArray(vao[PEDNERCIL]);
                                glMultiDrawElements(GL_TRIANGLE_FAN, cylCounts, GL_UNSIGNED_INT,(const void **)cylOffsets, CYL_LATS);
                            }
                        }else if(check[i][j]==4){
                             if(i==riga-1 && j==col-1 && selected==0){
                                glUniform1ui(objectLoc, PEDROSCIL);
                                glBindVertexArray(vao[PEDROSCIL]);
                                glMultiDrawElements(GL_TRIANGLE_FAN, cylCounts, GL_UNSIGNED_INT,(const void **)cylOffsets, CYL_LATS);
                            }else if(i==riga-1 && j==col-1 && selected==1){
                                glUniform1ui(objectLoc, PEDSELCIL);
                                glBindVertexArray(vao[PEDSELCIL]);
                                glMultiDrawElements(GL_TRIANGLE_FAN, cylCounts, GL_UNSIGNED_INT,(const void **)cylOffsets, CYL_LATS);
                            }
                            else
                            {
                                glUniform1ui(objectLoc, PEDVIOCIL);
                                glBindVertexArray(vao[PEDVIOCIL]);
                                glMultiDrawElements(GL_TRIANGLE_FAN, cylCounts, GL_UNSIGNED_INT,(const void **)cylOffsets, CYL_LATS);
                            }
                        } else if(check[i][j]==5){
                             if(i==riga-1 && j==col-1 && selected==0){
                                glUniform1ui(objectLoc, PEDROSCIL);
                                glBindVertexArray(vao[PEDROSCIL]);
                                glMultiDrawElements(GL_TRIANGLE_FAN, cylCounts, GL_UNSIGNED_INT,(const void **)cylOffsets, CYL_LATS);
                            }else if(i==riga-1 && j==col-1 && selected==1){
                                glUniform1ui(objectLoc, PEDSELCIL);
                                glBindVertexArray(vao[PEDSELCIL]);
                                glMultiDrawElements(GL_TRIANGLE_FAN, cylCounts, GL_UNSIGNED_INT,(const void **)cylOffsets, CYL_LATS);
                            }
                            else
                            {
                                glUniform1ui(objectLoc, PEDBLUCIL);
                                glBindVertexArray(vao[PEDBLUCIL]);
                                glMultiDrawElements(GL_TRIANGLE_FAN, cylCounts, GL_UNSIGNED_INT,(const void **)cylOffsets, CYL_LATS);
                            }
                        }  

                        glm_translate(modelViewMat, (vec3){0.0,0.0,0.0});
                        glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)(modelViewMat));
                        glUniform1ui(objectLoc, PEDBIABASE);
                        glBindVertexArray(vao[PEDBIABASE]);
                        glDrawArrays(GL_TRIANGLE_FAN, 0, DISC_SEGS);

                        glm_translate(modelViewMat, (vec3){-0.23,-0.25,0.0});
                        glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)(modelViewMat));
                        glUniform1ui(objectLoc, BOARD1);
                        glBindVertexArray(vao[BOARD1]);
                        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
                    }else{
                        glm_translate(modelViewMat, (vec3){-0.5,0.0,0.0});
                        glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)(modelViewMat));
                        glUniform1ui(objectLoc, BOARD1);
                        glBindVertexArray(vao[BOARD1]);
                        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
                    }
                }
            }

        }
    }

    glutSwapBuffers();
}

// OpenGL window reshape routine.
void resize(int w, int h)
{
    glViewport(0, 0, w, h);
    width = w;
    height = h;
}

void keyInput(unsigned char key, int x, int y){
    switch(key){

        //se si preme esc si esce dal programma
        case 27:
            exit (0);
        break;
   

        case 'i':     
            //quando il gioco inizia la posizione sarà sulla prima pedina

            riga=1;
            col=1;
        
            started=1;
            selected=0;

            //comando per resettare lo schema delle pedine 
            memcpy(check,inizio, 8*8*sizeof(int));

            //redisplay della scena con le modifiche apportate
            glutPostRedisplay();

        break;

        case 13:
            //seleziono la pedina
            if(selected==0){
                selected=1; 
            }else{
                selected=0;
            }
            
            glutPostRedisplay();
        break;

        case 'a':
            //tramite l'utilizzo di a ci spostiamo in diagonale a sinistra, nel caso del bianco verso l'alto
            //nel caso del nero si muove verso il basso
            if(started){
                //se la pedina non è selezionata
                if(selected==0){
                    //se è il turno è del player con le pedine bianche
                    if(turno==1){
                        //effettuo una ricerca per trovare la pedina bianca o la dama del bianco
                        for (int i=col-1; i>0; i--){
                            //se la pedina è bianca o è una dama del turno bianco mi salvo la colonna
                            if(check[riga-1][i-1]==1 || check[riga-1][i-1]==4){
                                col=i;
                                break;
                            }
                        }
                    }
                    //se è il turno del player con le pedine nere
                    else if(turno==2){
                        //effettuo una ricerca per trovare la pedina nera o la dama dei neri
                        for (int i=col-1; i>0; i--){
                            //se la pedina è nera o è una dama del turno nero mi salvo la colonna
                            if(check[riga-1][i-1]==2 || check[riga-1][i-1]==5){
                                col=i;
                                break;
                            }
                        }
                    }
                   
                }
                //se la pedina è stata selezionata 
                else if(selected==1){

                    //turno del bianco
                    if(turno==1){
                        //controllo se la posizione in digonale a sinistra successiva è libera
                        if(check[riga][col-2]==3){
                            if(riga==7 || check[riga-1][col-1]==4){
                                //nel caso la pedina che sto muovendo sia una dama ricreo la dama nella nuova posizione
                                check[riga][col-2]=4;
                            }else{
                                check[riga][col-2]=1;
                                 
                            }
                            //svuoto la casella attuale
                            check[riga-1][col-1]=3;
                            //deseleziono la posizione attuale e passo il turno quindi illumino la posione 8,8
                            selected=0;
                            turno=2;
                            riga=8;
                            col=8;
                        }
                        
                        //controllo salendo sulla diagonale sinistra abbiamo in sequenza una pedina nera e una casella vuota
                        else if((check[riga][col-2]==2 || check[riga][col-2]==5) && check[riga+1][col-3]==3){
                           
                            //creo la pedina bianca o la dama nella casella successiva in diagonale a quella precedentemente settata a vuota
            
                               if(check[riga-1][col-1]==4){
                                    check[riga+1][col-3]=4; 
                                }else{
                                    check[riga+1][col-3]=1;
                                }
                            
                            //setto la casella attuale vuota
                            check[riga-1][col-1]=3;
                            //svuoto la casella successiva in diagonale 
                            check[riga][col-2]=3;
                            

                            //passo il turno
                            selected=0;
                            turno=2;
                            riga=8;
                            col=8;
                        }
                    }

                    //turno dei neri
                    else if(turno==2){
                        //stesso movimento del turno bianco cambia la direzione: va verso il basso
                        if(check[riga-2][col-2]==3){
                            
                            if(riga==2 || check[riga-1][col-1]==5 ){
                                check[riga-2][col-2]=5;
                            }else{
                                check[riga-2][col-2]=2;
                            }
                            check[riga-1][col-1]=3;

                            selected=0;
                            turno=1;
                            riga=1;
                            col=1;
                        }
                     
                        else if((check[riga-2][col-2]==1 || check[riga-2][col-2]==4) && check[riga-3][col-3]==3){
                            
                            if(riga==3){
                                check[riga-3][col-3]=5;
                            }else{
                                if(check[riga-1][col-1]==5){
                                    check[riga-3][col-3]=5; 
                                }else{
                                    check[riga-3][col-3]=2;
                                }
                            }
                            
                            check[riga-1][col-1]=3;
                            check[riga-2][col-2]=3;
                            selected=0;
                            turno=1;
                            riga=1;
                            col=1;
                        }
                    }
                    
                }
            }
            glutPostRedisplay();
        break;

        case 'd':
        // nel caso si prema d ci spostiamo a destra in diagonale, nel caso bianco verso l'alto
        // nel caso nero si muove verso il basso

            if(started){
                //il funzionamento è uguale al pulsante a solo che si muove verso destra
                if(selected==0){
                    
                    //effettuiamo a ricerca della prima pedina libera
                    if(turno==1){

                        for (int i=col+1; i<=8; i++){
                            if(check[riga-1][i-1]==1 || check[riga-1][i-1]==4){
                                col=i;
                                break;
                            }
                        }
                    }else if(turno==2){
                        for (int i=col+1; i<=8; i++){
                            if(check[riga-1][i-1]==2 || check[riga-1][i-1]==5){
                                col=i;
                                break;
                            }
                        }
                    }
                    
                
                }else if(selected==1){
                    if(turno==1){
                        
                        //controllo se sulla scacchiera non c'è la pedina nella posizione successiva
                        if(check[riga][col]==3){
                            
                            if(riga==7 || check[riga-1][col-1]==4){
                                    check[riga][col]=4;
                                }else{
                                check[riga][col]=1; 
                                }
                            check[riga-1][col-1]=3;
                            
                            selected=0;
                            turno=2;
                            riga=8;
                            col=8;
                        }
                        //se nella casella successiva c'è un pedina nera o la dama del nero e la casella immediatamente successiva è vuota
                        //mettiamo la pedina bianca o la dama del bianco
                        else if((check[riga][col]==2 || check[riga][col]==5) && check[riga+1][col+1]==3){
                            if(riga==7){
                                check[riga+1][col+1]=4;
                            }else{
                                if(check[riga-1][col-1]==4){
                                    check[riga+1][col+1]=4;
                                }else{
                                    check[riga+1][col+1]=1;
                                }
                            }
                            check[riga-1][col-1]=3;
                            check[riga][col]=3;
                            
                            selected=0;
                            turno=2;
                            riga=8;
                            col=8;
                        }
                
                    }
                    else if(turno==2){
                        
                        if(check[riga-2][col]==3){
                            if(riga==2 || check[riga-1][col-1]==5){
                                check[riga-2][col]=5;
                            }else{
                                check[riga-2][col]=2;
                            }
                            check[riga-1][col-1]=3;
                            
                            selected=0;
                            turno=1;
                            riga=1;
                            col=1;
                        }
                    
                        else if((check[riga-2][col]==1 || check[riga-2][col]==4) && check[riga-3][col+1]==3){
                            if(riga==3){
                                check[riga-3][col+1]=5;
                            }else{
                                if(check[riga-1][col-1]==5){
                                    check[riga-1][col-1]=5;
                                }else{
                                check[riga-3][col+1]=2; 
                                }
                                
                            }
                            check[riga-1][col-1]=3;
                            check[riga-2][col]=3;
                            
                            selected=0;
                            turno=1;
                            riga=1;
                            col=1;
                        }
                        
                    }
                    
                }
            }
            glutPostRedisplay();
        break;

        case 'w':
            //si muove in su di una riga per selezionare
            if(started){
                //se la pedina non è selezionata
                if(selected==0){
                    bool stop;
                    if(turno==1){
                        for(int i=riga+1;i<=8 && !stop;i++){
                                for (int j=1; j<=8 && !stop; j++){
                                        if(check[i-1][j-1]==1 || check[i-1][j-1]==4){
                                                
                                                col=j;
                                                riga=i;
                                                stop=1;
                                            }
                                }
                            
                        }
                    }else if(turno==2){
                        for(int i=riga+1;i<=8 && !stop;i++){
                                for (int j=1; j<=8 && !stop; j++){
                                        if(check[i-1][j-1]==2 || check[i-1][j-1]==5){
                                                
                                                col=j;
                                                riga=i;
                                                stop=1;
                                        }
                                }
                        }
                    }
                }
            }
            glutPostRedisplay(); 
        break;


        case 's':
            //si muove in giù di una riga per selezionare
            if(started){
                //se la pedina non è selezionata
                if(selected==0){
                    bool stop;
                    if(turno==1){
                        for(int i=riga-1;i>0 && !stop;i--){
                                for (int j=1; j<=8 && !stop; j++){
                                    
                                        if(check[i-1][j-1]==1 || check[i-1][j-1]==4){
                                                
                                                col=j;
                                                riga=i;
                                                stop=1;
                                            }
                                }
                            
                        }
                    }else if(turno==2){
                        for(int i=riga-1;i>0 && !stop;i--){
                                for (int j=1; j<=8 && !stop; j++){
                                        if(check[i-1][j-1]==2 || check[i-1][j-1]==5){
                                                
                                                col=j;
                                                riga=i;
                                                stop=1;
                                            }
                                }
                            
                        }
                    }
                        
                    
                }
                
            }
            glutPostRedisplay(); 
        break;

        case 'z':
            //utilizziamo z solo in caso di dama per permettere il movimento opposto al movimento di a
            //ci muoviamo in diagonale a sinistra, verso il basso nel caso di dama bianca e verso l'alto nel caso
            //di dama nera
            if(started){
            if(turno==1 && check[riga-1][col-1]==4){
                        if(check[riga-2][col-2]==3){
                            check[riga-1][col-1]=3;
                            check[riga-2][col-2]=4;
                            selected=0;
                            turno=2;
                            riga=8;
                            col=8;
                        }
                        //controllo se sulla scacchiera (la posizione succesiva in obbliquo) è presente una pedina nera o una dama nera
                
                        else if((check[riga-2][col-2]==2|| check[riga-2][col-2]==5) && check[riga-3][col-3]==3){
                            check[riga-1][col-1]=3;
                            check[riga-2][col-2]=3;
                            check[riga-3][col-3]=4;
                            selected=0;
                            turno=2;
                            riga=8;
                            col=8;
                    }
                }else{
                    if(turno==2 && check[riga-1][col-1]==5){
                        if(check[riga][col-2]==3){
                            check[riga-1][col-1]=3;
                            check[riga][col-2]=5;
                            selected=0;
                            turno=1;
                            riga=1;
                            col=1;
                        }
                        //controllo se sulla scacchiera (la posizione succesiva in obbliquo) è presente una pedina bianca o una dama bianca
                    
                        else if((check[riga][col-2]==1 || check[riga][col-2]==4) && check[riga+1][col-3]==3){
                            check[riga-1][col-1]=3;
                            check[riga][col-2]=3;
                            check[riga+1][col-3]=5;
                            selected=0;
                            turno=1;
                            riga=1;
                            col=1;
                        }
                    }
                }
                    
            }
            
            glutPostRedisplay(); 

        break;

        case 'c':
            //utilizziamo c solo in caso di dama, ci muoviamo in diagonale a destra, verso l'alto nel caso della dama nera
            //verso il basso nel caso della dama bianca
            if(started){
                if(turno==1 && check[riga-1][col-1]==4){
                        if(check[riga-2][col]==3){
                            check[riga-1][col-1]=3;
                            check[riga-2][col]=4;
                            selected=0;
                            turno=2;
                            riga=8;
                            col=8;
                        }
                    
                        else if((check[riga-2][col]==2 || check[riga-2][col]==5) && check[riga-3][col+1]==3 && turno==1){
                            check[riga-1][col-1]=3;
                            check[riga-2][col]=3;
                            check[riga-3][col+1]=4;
                            selected=0;
                            turno=2;
                            riga=8;
                            col=8;
                        }
                }else{
                    if(turno==2 && check[riga-1][col-1]==5){
                        if(check[riga][col]==3){
                            check[riga-1][col-1]=3;
                            check[riga][col]=5;
                            selected=0;
                            turno=1;
                            riga=1;
                            col=1;
                        }
                        
                        else if((check[riga][col]==1 || check[riga][col]==4) && check[riga+1][col+1]==3){
                            check[riga-1][col-1]=3;
                            check[riga][col]=3;
                            check[riga+1][col+1]=5;
                            selected=0;
                            turno=1;
                            riga=1;
                            col=1;
                        }
                    }
                }
            }
            glutPostRedisplay();
        break;

    }
}



// Main routine.
int main(int argc, char **argv)
{
    GLenum glErr;


    glutInit(&argc, argv);
    glutInitContextVersion(4, 3);
    glutInitContextProfile(GLUT_CORE_PROFILE);
    glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);

    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
    glutInitWindowSize(3000, 1000);
    glutInitWindowPosition(100, 100);
 

    glutCreateWindow("DAMA 3D ALTERNATIVA");
    glutDisplayFunc(display);
    glutReshapeFunc(resize);
    glutKeyboardFunc(keyInput);
    glewInit();

    // ... it does not hurt to check that everything went OK
    if ((glErr = glGetError()) != 0)
    {
        printf("Errore = %d \n", glErr);
        exit(-1);
    }

    setup();
    glutMainLoop();

    return 0;
}
