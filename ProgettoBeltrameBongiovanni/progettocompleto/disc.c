#include <GL/glew.h>
#include <GL/freeglut.h>
#include <math.h>

#include "disc.h"
float x[4]={0.0,0.5,0.0,0.5};
float y[4]={0.0,0.0,0.5,0.5};

// Fill the vertex array with co-ordinates of the sample points.
void fillDiscVertexArray(Vertex discVertices[DISC_SEGS], int tipo)
{
   int k;
   //nel caso debba disegnare le caselle della scacchiera allora ricreo un quadrato
   if(tipo==1){
      for (k = 0; k < 4; k++)
         {
            discVertices[k].coords.x = x[k];
            discVertices[k].coords.y = y[k];
            discVertices[k].coords.z = 0.0;
            discVertices[k].coords.w = 1.0;
            discVertices[k].normal.x = 0.0;
            discVertices[k].normal.y = 0.0;
            discVertices[k].normal.z = 1.0;
            discVertices[k].texCoords.x = x[k];
            discVertices[k].texCoords.y = y[k];
         }
   }else
   //altrimenti ricreo la base della pedina 
    if (tipo==2) {
      for (k = 0; k < DISC_SEGS; k++)
         {
            discVertices[k].coords.x = 0.18 * cos( (-1 + 2*(float)k/DISC_SEGS) * M_PI );
            discVertices[k].coords.y = 0.18 * sin( (-1 + 2*(float)k/DISC_SEGS) * M_PI );
            discVertices[k].coords.z = 0.0;
            discVertices[k].coords.w = 1.0;
            discVertices[k].normal.x = 0.0;
            discVertices[k].normal.y = 0.0;
            discVertices[k].normal.z = 1.0;
            discVertices[k].texCoords.x = 0.5 + 0.5*cos( (-1 + 2*(float)k/DISC_SEGS) * M_PI );
            discVertices[k].texCoords.y = 0.5 + 0.5*sin( (-1 + 2*(float)k/DISC_SEGS) * M_PI );
         }
   }


}

