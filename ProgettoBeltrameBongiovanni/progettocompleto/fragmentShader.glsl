#version 430 core

#define CYLINDER 0
#define BOARD1 1
#define BOARD2 2
#define PEDBIABASE 3
#define PEDBIACIL 4
#define PEDNERCIL 5
#define PEDROSCIL 6
#define PEDBLUCIL 7
#define PEDVIOCIL 8
#define PEDSELCIL 9


// Material's properties
in vec4 frontAmbDiffExport, frontSpecExport, backAmbDiffExport, backSpecExport;
// texture coordinates
in vec2 texCoordsExport;

// samplers for the textures
uniform sampler2D biaTex;
uniform sampler2D marTex;
uniform sampler2D beiTex;
uniform sampler2D nerTex;
uniform sampler2D rosTex;
uniform sampler2D bluTex;
uniform sampler2D vioTex;



uniform uint object;

out vec4 colorsOut;

vec4 texColor;

void main(void)
{
    // check which object we are currently drawing
    if (object == CYLINDER) texColor = texture(nerTex, texCoordsExport);
    if (object == BOARD1) texColor = texture(beiTex, texCoordsExport);
    if (object == BOARD2) texColor = texture(marTex, texCoordsExport);
    if (object == PEDBIABASE) texColor = texture(beiTex, texCoordsExport);
    if (object == PEDBIACIL) texColor = texture(biaTex, texCoordsExport);
    if (object == PEDNERCIL) texColor = texture(nerTex, texCoordsExport);
    if (object == PEDROSCIL) texColor = texture(rosTex, texCoordsExport);
    if (object == PEDBLUCIL) texColor = texture(bluTex, texCoordsExport);
    if (object == PEDVIOCIL) texColor = texture(vioTex, texCoordsExport);
    if (object == PEDSELCIL) texColor = texture(marTex, texCoordsExport);
    // set color of the fragment
    colorsOut = gl_FrontFacing? (frontAmbDiffExport * texColor + frontSpecExport) :
                               (backAmbDiffExport * texColor + backSpecExport);
}
